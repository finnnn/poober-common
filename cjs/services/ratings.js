"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _lodash = require("lodash");

var _itsSet = _interopRequireDefault(require("its-set"));

var _users = _interopRequireDefault(require("./users"));

var _tables = _interopRequireDefault(require("../constants/tables"));

var _parse = require("../utils/parse");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _default = db => {
  const usersService = (0, _users.default)(db);

  const addMeta = async rating => {
    if (!rating) return null;
    const rater = await usersService.get(rating.rater);
    return { ...rating,
      rater
    };
  };

  return {
    find: async ({
      entity_id,
      entity_type,
      rater
    }) => {
      const query = db(_tables.default.RATINGS).select('*');

      if ((0, _itsSet.default)(entity_id)) {
        query.where('entity_id', (0, _parse.int)(entity_id));
      }

      if ((0, _itsSet.default)(entity_type)) {
        query.where('entity_type', entity_type);
      }

      if ((0, _itsSet.default)(rater)) {
        query.where('rater', (0, _parse.int)(rater));
      } // query.limit(10);


      const ratings = await query;
      return await Promise.all(ratings.map(addMeta));
    },
    create: async ({
      entity_type,
      entity_id,
      rater,
      rating,
      review
    }) => {
      const created_at = new Date();
      const [result] = await db(_tables.default.RATINGS).returning('*').insert({
        entity_type,
        entity_id,
        rater,
        rating,
        review,
        created_at,
        updated_at: created_at
      });
      return await addMeta(result);
    },
    get: async id => {
      const [rating] = await db(_tables.default.RATINGS).select('*').whereIn('id', ((0, _lodash.isArray)(id) ? (0, _lodash.uniq)(id) : [id]).map(_parse.int));
      return rating;
    },
    update: async (id, data) => {
      const [rating] = await db(_tables.default.RATINGS).returning('*').where({
        id: (0, _parse.int)(id)
      }).update({ ...data,
        updated_at: new Date()
      });
      return await addMeta(rating);
    },
    remove: async id => {
      await db(_tables.default.RATINGS).where({
        id: (0, _parse.int)(id)
      }).del();
    }
  };
};

exports.default = _default;
module.exports = exports.default;