"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _lodash = require("lodash");

var _itsSet = _interopRequireDefault(require("its-set"));

var _bookings = _interopRequireDefault(require("./bookings"));

var _tables = _interopRequireDefault(require("../constants/tables"));

var _notificationTypes = require("../constants/notificationTypes");

var _parse = require("../utils/parse");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _default = db => {
  const bookingsService = (0, _bookings.default)(db);

  const addMeta = async notification => {
    if (!notification) return null;
    const {
      type,
      payload
    } = notification;
    const jsonPayload = JSON.parse(payload);

    switch (type) {
      case _notificationTypes.INBOUND_BOOKING_REQUEST:
        {
          const booking = await bookingsService.get(jsonPayload.bookingId);
          return { ...notification,
            payload: { ...jsonPayload,
              booking
            }
          };
        }

      default:
        {
          return { ...notification,
            payload: jsonPayload
          };
        }
    }
  };

  return {
    find: async ({
      recipient,
      active
    }) => {
      const query = db(_tables.default.NOTIFICATIONS).select('*');

      if ((0, _itsSet.default)(recipient)) {
        query.where('recipient', (0, _parse.int)(recipient));
      }

      if ((0, _itsSet.default)(active)) {
        query.where('active', active);
      } // query.limit(10);


      const notifications = await query;
      return await Promise.all(notifications.map(addMeta));
    },
    create: async ({
      recipient,
      type,
      payload
    }) => {
      const created_at = new Date();
      const [result] = await db(_tables.default.NOTIFICATIONS).returning('*').insert({
        recipient,
        payload: JSON.stringify(payload),
        type,
        created_at,
        active: true,
        updated_at: created_at
      });
      return await addMeta(result);
    },
    get: async id => {
      const notifications = await db(_tables.default.NOTIFICATIONS).select('*').whereIn('id', ((0, _lodash.isArray)(id) ? (0, _lodash.uniq)(id) : [id]).map(_parse.int));

      if ((0, _lodash.isArray)(id)) {
        return await Promise.all(notifications.map(addMeta));
      } else {
        return await addMeta(notifications[0]);
      }
    },
    update: async (id, data) => {
      const query = db(_tables.default.NOTIFICATIONS).returning('*').update({ ...data,
        updated_at: new Date()
      });

      if ((0, _lodash.isArray)(id)) {
        const notifications = await query.whereIn('id', id.map(_parse.int));
        return await Promise.all(notifications.map(addMeta));
      } else {
        const [notification] = await query.where({
          id: (0, _parse.int)(id)
        });
        return await addMeta(notification);
      }
    },
    remove: async id => {
      await db(_tables.default.NOTIFICATIONS).where({
        id: (0, _parse.int)(id)
      }).del();
    }
  };
};

exports.default = _default;
module.exports = exports.default;