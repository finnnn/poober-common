"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _tables = _interopRequireDefault(require("../constants/tables"));

var _parse = require("../utils/parse");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const find = db => ({
  query
}) => db(_tables.default.BUSINESS_HOURS).select('*').where(query || {});

const create = db => async data => {
  const {
    entity_type,
    entity_id,
    timezone,
    day,
    opening_time,
    closing_time
  } = data;
  const created_at = new Date();
  const [result] = await db(_tables.default.BUSINESS_HOURS).returning('*').insert({
    entity_type,
    entity_id,
    timezone,
    day,
    opening_time,
    closing_time,
    created_at,
    updated_at: created_at
  });
  return result;
};

const update = db => async (id, data) => {
  const {
    entity_type,
    entity_id,
    timezone,
    day,
    opening_time,
    closing_time
  } = data;
  const [result] = await db(_tables.default.BUSINESS_HOURS).returning('*').where({
    id: (0, _parse.int)(id)
  }).update({
    entity_type,
    entity_id,
    timezone,
    day,
    opening_time,
    closing_time,
    updated_at: new Date()
  });
  return result;
};

var _default = db => ({
  find: find(db),
  create: create(db),
  update: update(db),
  createOrUpdate: async data => {
    const {
      entity_type,
      entity_id,
      day
    } = data;
    const [existing] = await find(db)({
      query: {
        entity_type,
        entity_id,
        day
      }
    });

    if (existing) {
      const [result] = await update(db)(existing.id, data);
      return result;
    }

    return await create(db)(data);
  },
  remove: async id => {
    await db(_tables.default.BUSINESS_HOURS).where({
      id: (0, _parse.int)(id)
    }).del();
  }
});

exports.default = _default;
module.exports = exports.default;