"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _lodash = require("lodash");

var _itsSet = _interopRequireDefault(require("its-set"));

var _nanoid = _interopRequireDefault(require("nanoid"));

var _parse = require("../utils/parse");

var _gis = require("../utils/gis");

var _tables = _interopRequireDefault(require("../constants/tables"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const DEFAULT_OPTIONS = {
  blacklist: ['password']
};

const omit = (object, blacklistKeys) => {
  const result = { ...object
  };
  blacklistKeys.forEach(key => {
    delete result[key];
  });
  return result;
};

var _default = db => ({
  create: async (payload, opts) => {
    const options = { ...DEFAULT_OPTIONS,
      ...opts
    };
    const now = new Date();
    const [user] = await db(_tables.default.USERS).returning(['*', db.postgis.asText('location')]).insert({
      phone_validated: false,
      phone_validation_code: (0, _nanoid.default)(),
      phone_validation_code_created_at: now,
      email_validated: false,
      email_validation_code: (0, _nanoid.default)(),
      email_validation_code_created_at: now,
      created_at: now,
      updated_at: now,
      ...payload
    });
    return omit(user, options.blacklist);
  },
  find: async (filters, opts) => {
    const options = { ...DEFAULT_OPTIONS,
      ...opts
    };
    const users = await db(_tables.default.USERS).select(['*', db.postgis.asText('location')]).where(filters);
    return users.map(user => omit(user, options.blacklist));
  },
  get: async (id, opts) => {
    const options = { ...DEFAULT_OPTIONS,
      ...opts
    };
    const multiple = (0, _lodash.isArray)(id);
    const users = await db(_tables.default.USERS).select(['*', db.postgis.asText('location')]).whereIn('id', (multiple ? (0, _lodash.uniq)(id) : [id]).map(_parse.int));

    if (multiple) {
      return users.map(user => omit(user, options.blacklist));
    } else {
      const [user] = users;
      if (!user) return user;
      return omit(user, options.blacklist);
    }
  },
  update: async (id, data, opts) => {
    const options = { ...DEFAULT_OPTIONS,
      ...opts
    };
    const payload = { ...data,
      updated_at: new Date()
    };

    if ((0, _itsSet.default)(data.location)) {
      payload.location = (0, _gis.jsonToPoint)({
        db,
        point: data.location
      });
    }

    const [user] = await db(_tables.default.USERS).returning(['*', db.postgis.asText('location')]).where({
      id: (0, _parse.int)(id)
    }).update(payload);
    return omit(user, options.blacklist);
  },
  remove: async id => {
    await db(_tables.default.USERS).where({
      id: (0, _parse.int)(id)
    }).del();
  }
});

exports.default = _default;
module.exports = exports.default;