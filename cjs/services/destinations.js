"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _lodash = require("lodash");

var _itsSet = _interopRequireDefault(require("its-set"));

var _moment = _interopRequireDefault(require("moment"));

var _tables = _interopRequireDefault(require("../constants/tables"));

var _gis = require("../utils/gis");

var _businessHours = _interopRequireDefault(require("./businessHours"));

var _users = _interopRequireDefault(require("./users"));

var _parse = require("../utils/parse");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// import moment from 'moment';
// import { dateToMinsFromMidnight, dayOfWeek } from '../utils/time';
// import bookingsServiceFactory from './bookings';
var _default = db => {
  const businessHoursService = (0, _businessHours.default)(db);
  const usersService = (0, _users.default)(db);

  const addMetaToOneDestination = async destination => {
    if (!destination) return null;
    const owner = await usersService.get(destination.owner);
    const businessHours = await businessHoursService.find({
      query: {
        entity_type: 'destination',
        entity_id: destination.id
      }
    });
    const now = (0, _moment.default)();
    return { ...destination,
      business_hours: businessHours.reduce((acc, hours) => ({ ...acc,
        [(0, _parse.int)(hours.day)]: {
          closing_time: hours.closing_time,
          opening_time: hours.opening_time,
          timezone: hours.timezone
        }
      }), {}),
      owner,
      soonest_availability: destination.soonest_availability && (0, _moment.default)(destination.soonest_availability).isAfter(now) ? (0, _moment.default)(destination.soonest_availability).toDate() : null
    };
  };

  const addMeta = async destination => (0, _lodash.isArray)(destination) ? await Promise.all(destination.map(d => addMetaToOneDestination(d))) : await addMetaToOneDestination(destination);

  const destinationsSelect = (callback = () => {}) => {
    const soonestAvailabilityQuery = db(_tables.default.BOOKINGS).first().select('end_at').whereRaw(`destination = ${_tables.default.DESTINATIONS}.id`).andWhere({
      active: true
    }).orderBy('end_at', 'desc').as('soonest_availability');
    return db.with('destinations_enriched', qb => {
      qb.select(`${_tables.default.DESTINATIONS}.*`, db.postgis.asText('location'), soonestAvailabilityQuery).from(_tables.default.DESTINATIONS).groupBy(`${_tables.default.DESTINATIONS}.id`);
      callback(qb);
    }).select('*').orderBy('soonest_availability').from('destinations_enriched');
  };

  return {
    find: async ({
      origin,
      // begin_at,
      // end_at,
      // timezone,
      limit,
      owner,
      urgent,
      not_owner
    }) => {
      const destinationsQuery = destinationsSelect(qb => {
        if ((0, _itsSet.default)(origin)) {
          qb.orderByRaw(`location <-> st_setsrid(st_makepoint(${origin}),4326)`);
        }

        if ((0, _itsSet.default)(owner)) {
          qb.andWhere({
            owner: (0, _parse.int)(owner)
          });
        }

        if ((0, _itsSet.default)(not_owner)) {
          qb.whereNot({
            owner: (0, _parse.int)(not_owner)
          });
        }

        if ((0, _itsSet.default)(urgent)) {
          qb.andWhere({
            auto_accept_bookings: true
          });
        }
      });
      destinationsQuery.limit((0, _parse.int)(limit) || 100);
      const destinations = await destinationsQuery;
      return await addMeta(destinations);
    },
    create: async data => {
      const {
        name,
        location,
        auto_accept_bookings,
        owner,
        business_hours
      } = data;
      const created_at = new Date();
      const [destination] = await db(_tables.default.DESTINATIONS).returning(['*', db.postgis.asText('location')]).insert({
        name,
        location: (0, _gis.jsonToPoint)({
          db,
          point: location
        }),
        auto_accept_bookings,
        created_at,
        updated_at: created_at,
        owner
      });

      if ((0, _itsSet.default)(business_hours)) {
        await Promise.all(Object.keys(business_hours).map(day => businessHoursService.create({
          entity_type: 'destination',
          entity_id: destination.id,
          timezone: business_hours[day].timezone,
          day: (0, _parse.int)(day),
          opening_time: business_hours[day].opening_time,
          closing_time: business_hours[day].closing_time
        })));
      }

      const [updatedDestination] = await destinationsSelect(qb => {
        qb.where({
          id: (0, _parse.int)(destination.id)
        });
      });
      return addMeta(updatedDestination);
    },
    get: async id => {
      const [destination] = await destinationsSelect(qb => {
        qb.where({
          id: (0, _parse.int)(id)
        });
      });
      return addMeta(destination);
    },
    update: async (id, data) => {
      const {
        name,
        location,
        auto_accept_bookings,
        business_hours
      } = data;
      const [destination] = await db(_tables.default.DESTINATIONS).returning(['*', db.postgis.asText('location')]).where({
        id: (0, _parse.int)(id)
      }).update({
        name,
        location: (0, _gis.jsonToPoint)({
          db,
          point: location
        }),
        auto_accept_bookings,
        updated_at: new Date()
      });

      if ((0, _itsSet.default)(business_hours)) {
        const previousBusinessHours = await businessHoursService.find({
          entity_type: 'destination',
          entity_id: destination.id
        });
        const currentBusinessHours = await Promise.all(Object.keys(business_hours).map(day => businessHoursService.createOrUpdate({
          entity_type: 'destination',
          entity_id: destination.id,
          timezone: business_hours[day].timezone,
          day: (0, _parse.int)(day),
          opening_time: business_hours[day].opening_time,
          closing_time: business_hours[day].closing_time
        })));
        const daysToBeRemoved = previousBusinessHours.filter(hours => !currentBusinessHours.map(({
          day
        }) => day).includes(hours.day));
        await Promise.all(daysToBeRemoved.map(({
          id
        }) => businessHoursService.remove(id)));
      }

      const [updatedDestination] = await destinationsSelect(qb => {
        qb.where({
          id: (0, _parse.int)(id)
        });
      });
      return addMeta(updatedDestination);
    },
    remove: async id => {
      await db(_tables.default.DESTINATIONS).where({
        id: (0, _parse.int)(id)
      }).del();
    }
  };
};

exports.default = _default;
module.exports = exports.default;