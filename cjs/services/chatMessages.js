"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _tables = _interopRequireDefault(require("../constants/tables"));

var _users = _interopRequireDefault(require("./users"));

var _parse = require("../utils/parse");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _default = db => {
  const usersService = (0, _users.default)(db);

  const formatMessage = async message => {
    if (!message) return null;
    const sender = await usersService.get(message.sender);
    const recipient = await usersService.get(message.recipient);
    return { ...message,
      sender,
      recipient
    };
  };

  const formatMessages = async messages => {
    const users = await usersService.get(messages.reduce((acc, {
      sender,
      recipient
    }) => acc.concat([sender, recipient]), []));
    return messages.map(message => {
      return { ...message,
        sender: users.filter(({
          id
        }) => id === message.sender)[0],
        recipient: users.filter(({
          id
        }) => id === message.recipient)[0]
      };
    });
  };

  return {
    find: async ({
      userA,
      userB
    }) => {
      const query = db(_tables.default.CHAT_MESSAGES).select('*');
      query.andWhere(function () {
        this.orWhere({
          recipient: (0, _parse.int)(userA),
          sender: (0, _parse.int)(userB)
        });
        this.orWhere({
          recipient: (0, _parse.int)(userB),
          sender: (0, _parse.int)(userA)
        });
      }); // query.limit(limit);

      const messages = await query;
      return formatMessages(messages);
    },
    create: async data => {
      const {
        text,
        sender,
        recipient,
        created_at
      } = data;
      const createdAt = created_at || new Date();
      const [result] = await db(_tables.default.CHAT_MESSAGES).returning('*').insert({
        text,
        sender,
        recipient,
        created_at: createdAt,
        updated_at: createdAt
      });
      return formatMessage(result);
    },
    get: async id => {
      const [message] = await db(_tables.default.CHAT_MESSAGES).select().where({
        id: (0, _parse.int)(id)
      });
      return formatMessage(message);
    },
    update: async (id, {
      text
    }) => {
      const [message] = await db(_tables.default.CHAT_MESSAGES).returning('*').where({
        id: (0, _parse.int)(id)
      }).update({
        text,
        updated_at: new Date()
      });
      return formatMessage(message);
    },
    remove: async id => await db(_tables.default.CHAT_MESSAGES).where({
      id: (0, _parse.int)(id)
    }).del()
  };
};

exports.default = _default;
module.exports = exports.default;