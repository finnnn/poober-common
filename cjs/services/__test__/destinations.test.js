"use strict";

const {
  test
} = require('tap');

const moment = require('moment');

const db = require('../../utils/db');

const gis = require('../../utils/gis');

const service = require('../destinations')(db);

const TABLES = require('../../constants/tables'); // const { dateToMinsFromMidnight, dayOfWeek, getTimezone } = require('../../utils/time');


const {
  roundDate
} = require('../../utils/test');

const testData = {};
const currentTime = new Date();
test('services/destinations', async t => {
  t.test('intialize db', async t => {
    db.initialize();
    t.end();
  });
  t.test('truncate tables', async t => {
    await db(TABLES.DESTINATIONS).truncate();
    await db(TABLES.BOOKINGS).truncate();
    await db(TABLES.BUSINESS_HOURS).truncate();
    t.end();
  });
  t.test('seed data', async t => {
    const [destA] = await db(TABLES.DESTINATIONS).returning('*').insert({
      name: `DEST A`,
      location: gis.jsonToPoint({
        db,
        point: {
          lat: 2,
          lon: 0
        }
      }),
      created_at: currentTime,
      updated_at: currentTime
    }); // const [destB] = await db(TABLES.DESTINATIONS)
    //   .returning('*')
    //   .insert({
    //     name: `DEST B`,
    //     location: gis.jsonToPoint({ db, point: { lat: 1, lon: 0 } }),
    //     created_at: currentTime,
    //     updated_at: currentTime,
    //   });

    const [destC] = await db(TABLES.DESTINATIONS).returning('*').insert({
      name: `DEST C`,
      location: gis.jsonToPoint({
        db,
        point: {
          lat: 3,
          lon: 0
        }
      }),
      created_at: currentTime,
      updated_at: currentTime
    }); // const [destD] = await db(TABLES.DESTINATIONS)
    //   .returning('*')
    //   .insert({
    //     name: `DEST D`,
    //     location: gis.jsonToPoint({ db, point: { lat: 5, lon: 0 } }),
    //     created_at: currentTime,
    //     updated_at: currentTime,
    //   });
    // const [destE] = await db(TABLES.DESTINATIONS)
    //   .returning('*')
    //   .insert({
    //     name: `DEST E`,
    //     location: gis.jsonToPoint({ db, point: { lat: 6, lon: 0 } }),
    //     created_at: currentTime,
    //     updated_at: currentTime,
    //   });
    // const [destF] = await db(TABLES.DESTINATIONS)
    //   .returning('*')
    //   .insert({
    //     name: `DEST F`,
    //     location: gis.jsonToPoint({ db, point: { lat: 7, lon: 0 } }),
    //     created_at: currentTime,
    //     updated_at: currentTime,
    //   });
    // // DestB bookings - either side
    // const destBBookings = await Promise.all([
    //   db(TABLES.BOOKINGS).insert({
    //     destination: destB.id,
    //     begin_at: moment(currentTime).subtract(20, 'minutes').toDate(),
    //     end_at: moment(currentTime).subtract(5, 'minutes').toDate(),
    //     created_at: currentTime,
    //     updated_at: currentTime,
    //   }),
    //   db(TABLES.BOOKINGS).insert({
    //     destination: destB.id,
    //     begin_at: moment(currentTime).add(20, 'minutes').toDate(),
    //     end_at: moment(currentTime).add(35, 'minutes').toDate(),
    //     created_at: currentTime,
    //     updated_at: currentTime,
    //   }),
    // ]);
    // DestC bookings - overlapping left

    const destCBookings = await db(TABLES.BOOKINGS).insert({
      destination: destC.id,
      begin_at: moment(currentTime).subtract(10, 'minutes').toDate(),
      end_at: moment(currentTime).add(5, 'minutes').toDate(),
      created_at: currentTime,
      updated_at: currentTime
    }); // // DestD bookings - overlapping right
    // const destDBookings = await db(TABLES.BOOKINGS).insert({
    //   destination: destD.id,
    //   begin_at: moment(currentTime).add(10, 'minutes').toDate(),
    //   end_at: moment(currentTime).add(25, 'minutes').toDate(),
    //   created_at: currentTime,
    //   updated_at: currentTime,
    // });
    // // DestE bookings - contained
    // const destEBookings = await db(TABLES.BOOKINGS).insert({
    //   destination: destE.id,
    //   begin_at: moment(currentTime).add(5, 'minutes').toDate(),
    //   end_at: moment(currentTime).add(10, 'minutes').toDate(),
    //   created_at: currentTime,
    //   updated_at: currentTime,
    // });
    // // DestE bookings - contained
    // const destBHours = await db(TABLES.BUSINESS_HOURS).insert({
    //   entity_type: 'destination',
    //   entity_id: destB.id,
    //   opening_time: dateToMinsFromMidnight(moment(currentTime).subtract(5, 'minutes')),
    //   closing_time: dateToMinsFromMidnight(moment(currentTime).add(60, 'minutes').toDate()),
    //   timezone: getTimezone(currentTime),
    //   day: dayOfWeek(currentTime),
    //   created_at: currentTime,
    //   updated_at: currentTime,
    // });
    // // DestF bookings - contained
    // const destFHours = await db(TABLES.BUSINESS_HOURS).insert({
    //   entity_type: 'destination',
    //   entity_id: destF.id,
    //   opening_time: dateToMinsFromMidnight(moment(currentTime).add(5, 'minutes')),
    //   closing_time: dateToMinsFromMidnight(moment(currentTime).add(60, 'minutes').toDate()),
    //   timezone: getTimezone(currentTime),
    //   day: dayOfWeek(currentTime),
    //   created_at: currentTime,
    //   updated_at: currentTime,
    // });

    testData.destA = destA; // testData.destB = destB;

    testData.destC = destC; // testData.destD = destD;
    // testData.destE = destE;
    // testData.destF = destF;
    // testData.destBBookings = destBBookings;

    testData.destCBookings = destCBookings; // testData.destDBookings = destDBookings;
    // testData.destEBookings = destEBookings;

    t.end();
  });
  t.test('find()', async t => {
    const dests = await service.find({});
    t.equal(dests.length, 2);
    const destsWithOrigin = await service.find({
      origin: '3,0',
      limit: 2
    });
    t.equal(destsWithOrigin.length, 2);
    t.match(destsWithOrigin, [{
      name: `DEST C`,
      created_at: currentTime,
      updated_at: currentTime
    }, {
      name: `DEST A`,
      created_at: currentTime,
      updated_at: currentTime
    }]);
    t.end();
  });
  t.test('truncate tables', async t => {
    await db(TABLES.BOOKINGS).truncate();
    t.end();
  });
  t.test('soonest_availability', async t => {
    const currentTime = new Date();
    const destABookingA = await db(TABLES.BOOKINGS).insert({
      destination: testData.destA.id,
      begin_at: moment(currentTime).subtract(100, 'minutes').toDate(),
      end_at: moment(currentTime).subtract(80, 'minutes').toDate(),
      created_at: moment(currentTime).subtract(180, 'minutes').toDate(),
      updated_at: moment(currentTime).subtract(180, 'minutes').toDate()
    });
    testData.destABookingA = destABookingA;
    const destCBookingA = await db(TABLES.BOOKINGS).insert({
      destination: testData.destC.id,
      begin_at: moment(currentTime).subtract(10, 'minutes').toDate(),
      end_at: moment(currentTime).add(5, 'minutes').toDate(),
      created_at: moment(currentTime).subtract(20, 'minutes').toDate(),
      updated_at: moment(currentTime).subtract(20, 'minutes').toDate()
    });
    testData.destCBookingA = destCBookingA;
    const destCBookingB = await db(TABLES.BOOKINGS).insert({
      destination: testData.destC.id,
      begin_at: moment(currentTime).add(5, 'minutes').toDate(),
      end_at: moment(currentTime).add(25, 'minutes').toDate(),
      created_at: currentTime,
      updated_at: currentTime
    });
    testData.destCBookingB = destCBookingB;
    const destA = await service.get(testData.destA.id);
    const destC = await service.get(testData.destC.id);
    t.equal(destA.soonest_availability, null);
    t.equal(roundDate(moment(currentTime).add(25, 'minutes').toDate()), roundDate(destC.soonest_availability));
    t.end();
  });
  t.test('close DB', t => {
    db.destroy(() => {
      t.end();
    });
  });
  t.end();
});