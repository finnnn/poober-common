"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _lodash = require("lodash");

var _tables = _interopRequireDefault(require("../constants/tables"));

var _parse = require("../utils/parse");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _default = db => ({
  find: async ({
    user
  }) => {
    const query = db(_tables.default.SETTINGS_NOTIFICATIONS).select('*').where('user', user);
    const settingsNotifications = await query;
    return settingsNotifications;
  },
  create: async ({
    user
  }) => {
    const [result] = await db(_tables.default.SETTINGS_NOTIFICATIONS).returning('*').insert({
      user
    });
    return result;
  },
  get: async id => {
    const [settingNotification] = await db(_tables.default.SETTINGS_NOTIFICATIONS).select('*').whereIn('id', ((0, _lodash.isArray)(id) ? (0, _lodash.uniq)(id) : [id]).map(_parse.int));
    return settingNotification;
  },
  remove: async id => {
    await db(_tables.default.SETTINGS_NOTIFICATIONS).where({
      id: (0, _parse.int)(id)
    }).del();
  }
});

exports.default = _default;
module.exports = exports.default;