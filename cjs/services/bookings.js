"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _moment = _interopRequireDefault(require("moment"));

var _itsSet = _interopRequireDefault(require("its-set"));

var _lodash = require("lodash");

var _tables = _interopRequireDefault(require("../constants/tables"));

var _bookingStatuses = require("../constants/bookingStatuses");

var _destinations = _interopRequireDefault(require("./destinations"));

var _users = _interopRequireDefault(require("./users"));

var _parse = require("../utils/parse");

var _timing = require("../constants/timing");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _default = db => {
  const destinationsService = (0, _destinations.default)(db);
  const usersService = (0, _users.default)(db);

  const addMeta = async booking => {
    if (!booking) return null;
    const destination = await destinationsService.get(booking.destination);
    const requester = await usersService.get(booking.requester);
    return { ...booking,
      destination,
      requester
    };
  };

  return {
    find: async ({
      requester,
      host,
      active,
      destination,
      day,
      begin_at,
      end_at,
      created_at
    }) => {
      const query = db(_tables.default.BOOKINGS).select(`${_tables.default.BOOKINGS}.*`);

      if ((0, _itsSet.default)(day)) {
        query.where('day', day);
      }

      if ((0, _itsSet.default)(destination)) {
        query.where('destination', (0, _parse.int)(destination));
      }

      if ((0, _itsSet.default)(requester)) {
        query.where('requester', (0, _parse.int)(requester));
      }

      if ((0, _itsSet.default)(host)) {
        query.join(_tables.default.DESTINATIONS, `${_tables.default.DESTINATIONS}.id`, `${_tables.default.BOOKINGS}.destination`).where(`${_tables.default.DESTINATIONS}.owner`, (0, _parse.int)(host));
      }

      if ((0, _itsSet.default)(active)) {
        query.where('active', active);
      }

      if ((0, _itsSet.default)(begin_at)) {
        if ((0, _lodash.isArray)(begin_at)) {
          query.where('begin_at', ...begin_at);
        } else {
          query.where('begin_at', begin_at);
        }
      }

      if ((0, _itsSet.default)(end_at)) {
        if ((0, _lodash.isArray)(end_at)) {
          query.where('end_at', ...end_at);
        } else {
          query.where('end_at', end_at);
        }
      }

      if ((0, _itsSet.default)(created_at)) {
        if ((0, _lodash.isArray)(created_at)) {
          query.where('created_at', ...created_at);
        } else {
          query.where('created_at', created_at);
        }
      } // query.limit(10);


      const bookings = await query;
      return Promise.all(bookings.map(addMeta));
    },
    create: async data => {
      const {
        requester,
        begin_at,
        end_at,
        destination,
        status = _bookingStatuses.APPROVAL_PENDING
      } = data;
      const created_at = new Date();
      const [result] = await db(_tables.default.BOOKINGS).returning('*').insert({
        requester,
        begin_at,
        end_at,
        begin_day: (0, _moment.default)(begin_at).day(),
        destination,
        status,
        created_at,
        updated_at: created_at,
        active: [_bookingStatuses.APPROVAL_PENDING, _bookingStatuses.COMMENCEMENT_PENDING, _bookingStatuses.COMMENCEMENT_SUCCESSFUL, _bookingStatuses.EXECUTION_SUCCESSFUL].includes(status)
      });
      return addMeta(result);
    },
    get: async id => {
      const [booking] = await db(_tables.default.BOOKINGS).select(`${_tables.default.BOOKINGS}.*`).where({
        id: (0, _parse.int)(id)
      });
      return addMeta(booking);
    },
    update: async (id, data) => {
      const payload = { ...data,
        updated_at: new Date()
      };

      if ((0, _itsSet.default)(data.status)) {
        payload.active = [_bookingStatuses.APPROVAL_PENDING, _bookingStatuses.COMMENCEMENT_PENDING, _bookingStatuses.COMMENCEMENT_SUCCESSFUL].includes(data.status);
      }

      if ((0, _itsSet.default)(data.begin_at)) {
        payload.begin_day = (0, _moment.default)(data.begin_at).day();
        payload.end_at = (0, _moment.default)(data.begin_at).add(_timing.DEFAULT_BOOKING_PERIOD_MINUTES, 'minutes').toDate();
      }

      const bookingsQuery = db(_tables.default.BOOKINGS).returning('*');
      const multiple = (0, _lodash.isArray)(id);

      if (multiple) {
        bookingsQuery.whereIn('id', id.map(i => (0, _parse.int)(i)));
      } else {
        bookingsQuery.where({
          id: (0, _parse.int)(id)
        });
      }

      const bookings = await bookingsQuery.update(payload);
      return await (multiple ? Promise.all(bookings.map(booking => addMeta(booking))) : addMeta(bookings[0]));
    },
    remove: async id => await db(_tables.default.BOOKINGS).where({
      id: (0, _parse.int)(id)
    }).del()
  };
};

exports.default = _default;
module.exports = exports.default;