"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
const TERRIBLE = 'TERRIBLE';
const BAD = 'BAD';
const DECENT = 'DECENT';
const GOOD = 'GOOD';
const AWESOME = 'AWESOME';
var _default = {
  TERRIBLE,
  BAD,
  DECENT,
  GOOD,
  AWESOME
};
exports.default = _default;
module.exports = exports.default;