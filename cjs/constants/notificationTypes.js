"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.INBOUND_BOOKING_REQUEST = void 0;
const INBOUND_BOOKING_REQUEST = 'INBOUND_BOOKING_REQUEST';
exports.INBOUND_BOOKING_REQUEST = INBOUND_BOOKING_REQUEST;
var _default = {
  INBOUND_BOOKING_REQUEST
};
exports.default = _default;