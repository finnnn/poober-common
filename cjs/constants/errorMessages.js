"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.BOOKING_STATUS_CHANGE = void 0;

var _bookingStatuses = require("poober-common/cjs/constants/bookingStatuses");

const BOOKING_STATUS_CHANGE = {
  [_bookingStatuses.APPROVAL_DECLINED]: 'The host has declined your request. Select again from venues that will automatically accept your booking.',
  [_bookingStatuses.APPROVAL_EXPIRED]: 'The host is unresponsive. Select again from venues that will automatically accept your booking.',
  [_bookingStatuses.COMMENCEMENT_PROBLEMATIC]: 'Our condolences that you are having troubles. Select again from venues that will automatically accept your booking.'
};
exports.BOOKING_STATUS_CHANGE = BOOKING_STATUS_CHANGE;