"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.SETTINGS_NOTIFICATIONS = exports.USERS = exports.RATINGS = exports.NOTIFICATIONS = exports.MIGRATIONS = exports.DESTINATIONS = exports.CHAT_MESSAGES = exports.BUSINESS_HOURS = exports.BOOKINGS = void 0;
const BOOKINGS = 'public.bookings';
exports.BOOKINGS = BOOKINGS;
const BUSINESS_HOURS = 'public.business_hours';
exports.BUSINESS_HOURS = BUSINESS_HOURS;
const CHAT_MESSAGES = 'public.chat_messages';
exports.CHAT_MESSAGES = CHAT_MESSAGES;
const DESTINATIONS = 'public.destinations';
exports.DESTINATIONS = DESTINATIONS;
const MIGRATIONS = 'public.migrations';
exports.MIGRATIONS = MIGRATIONS;
const NOTIFICATIONS = 'public.notifications';
exports.NOTIFICATIONS = NOTIFICATIONS;
const RATINGS = 'public.ratings';
exports.RATINGS = RATINGS;
const USERS = 'public.users';
exports.USERS = USERS;
const SETTINGS_NOTIFICATIONS = 'public.user_has_notification_types';
exports.SETTINGS_NOTIFICATIONS = SETTINGS_NOTIFICATIONS;
var _default = {
  BOOKINGS,
  BUSINESS_HOURS,
  CHAT_MESSAGES,
  DESTINATIONS,
  MIGRATIONS,
  NOTIFICATIONS,
  RATINGS,
  SETTINGS_NOTIFICATIONS,
  USERS
};
exports.default = _default;