"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
const LOW = 'LOW';
const MODERATE = 'MODERATE';
const DEFAULT = 'DEFAULT';
const HIGH = 'HIGH';
const CRITICAL = 'CRITICAL';
var _default = {
  LOW,
  MODERATE,
  DEFAULT,
  HIGH,
  CRITICAL
};
exports.default = _default;
module.exports = exports.default;