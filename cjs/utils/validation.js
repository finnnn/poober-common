"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.socket = exports.headers = exports.body = exports.query = void 0;

var _joi = _interopRequireDefault(require("joi"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const query = schema => (req, res, next) => {
  _joi.default.validate(req.query, schema, {
    allowUnknown: true
  }, next);
};

exports.query = query;

const body = schema => (req, res, next) => {
  _joi.default.validate(req.body, schema, {}, next);
};

exports.body = body;

const headers = schema => (req, res, next) => {
  _joi.default.validate(req.headers, schema, {
    allowUnknown: true
  }, next);
};

exports.headers = headers;

const socket = (schema, payload, next) => {
  _joi.default.validate(payload, schema, {}, next);
};

exports.socket = socket;