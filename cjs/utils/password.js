"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.testPasswordStrength = void 0;

var _owaspPasswordStrengthTest = _interopRequireDefault(require("owasp-password-strength-test"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_owaspPasswordStrengthTest.default.config({
  allowPassphrases: true,
  maxLength: 128,
  minLength: 10,
  minPhraseLength: 20,
  minOptionalTestsToPass: 4
});

const testPasswordStrength = password => _owaspPasswordStrengthTest.default.test(password);

exports.testPasswordStrength = testPasswordStrength;