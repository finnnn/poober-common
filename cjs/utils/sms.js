"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.send = exports.init = void 0;

var _twilio = _interopRequireDefault(require("twilio"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

let client;
let from;

const init = ({
  sid,
  token,
  phoneNumber
}) => {
  client = (0, _twilio.default)(sid, token);
  from = phoneNumber;
  return client;
};

exports.init = init;

const send = ({
  body,
  to
}) => new Promise(resolve => {
  if (!client) {
    resolve();
    return;
  }

  client.messages.create({
    body,
    to,
    from
  }).then(resolve).done();
});

exports.send = send;