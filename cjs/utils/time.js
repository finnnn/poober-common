"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.humanReadableTime = exports.getTimezone = exports.dayOfWeek = exports.dateToMinsFromMidnight = void 0;

var _moment = _interopRequireDefault(require("moment"));

var _itsSet = _interopRequireDefault(require("its-set"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const dateToMinsFromMidnight = date => {
  const d = (0, _moment.default)(date);
  return d.get('minute') + d.get('hour') * 60;
};

exports.dateToMinsFromMidnight = dateToMinsFromMidnight;

const dayOfWeek = date => (0, _moment.default)(date).format('d');

exports.dayOfWeek = dayOfWeek;

const getTimezone = date => (0, _moment.default)(date).format('ZZ');

exports.getTimezone = getTimezone;

const humanReadableTime = minsFromMidnight => {
  if (!(0, _itsSet.default)(minsFromMidnight)) return null;
  const hours = Math.floor(minsFromMidnight / 60);
  const hourMinutes = hours * 60;
  const minutes = minsFromMidnight - hourMinutes;
  return (0, _moment.default)().set('hour', hours).set('minute', minutes).format('HH:mm');
};

exports.humanReadableTime = humanReadableTime;