"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.syncPeerBookings = exports.syncTimeWindow = exports.canTransitionToStatus = void 0;

var _moment = _interopRequireDefault(require("moment"));

var _bookingStatuses = require("../constants/bookingStatuses");

var _timing = require("../constants/timing");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const bookingsServiceFactory = require('../services/bookings');

const canTransitionToStatus = ({
  booking,
  status,
  userId
}) => {
  const outbound = booking.requester.id === userId;
  const inbound = booking.destination.owner.id === userId;

  switch (booking.status) {
    case _bookingStatuses.APPROVAL_PENDING:
      {
        if (outbound) {
          if ([_bookingStatuses.CANCELLED].includes(status)) {
            return true;
          }

          return false;
        }

        if ([_bookingStatuses.COMMENCEMENT_PENDING, _bookingStatuses.APPROVAL_DECLINED].includes(status)) {
          return true;
        }

        break;
      }

    case _bookingStatuses.APPROVAL_DECLINED:
    case _bookingStatuses.APPROVAL_EXPIRED:
      {
        return false;
      }

    case _bookingStatuses.COMMENCEMENT_PENDING:
      {
        if (inbound) return false;

        if ([_bookingStatuses.COMMENCEMENT_SUCCESSFUL, _bookingStatuses.COMMENCEMENT_PROBLEMATIC, _bookingStatuses.CANCELLED].includes(status)) {
          return true;
        }

        break;
      }

    case _bookingStatuses.COMMENCEMENT_SUCCESSFUL:
      {
        if (inbound) return false;

        if ([_bookingStatuses.EXECUTION_SUCCESSFUL].includes(status)) {
          return true;
        }

        break;
      }

    case _bookingStatuses.EXECUTION_SUCCESSFUL:
      {
        if (inbound) return false;

        if ([_bookingStatuses.CLOSED].includes(status)) {
          return true;
        }

        break;
      }
    // no default
  }

  return false;
};

exports.canTransitionToStatus = canTransitionToStatus;

const syncTimeWindow = (booking, to, opts) => {
  const {
    offset
  } = {
    offset: 10,
    ...opts
  };
  const beginAt = (0, _moment.default)(to).add(offset, 'minutes');
  const period = (0, _moment.default)(booking.end_at).diff(booking.begin_at, 'minutes');
  const end_at = beginAt.clone().add(period, 'minutes').toDate();
  return { ...booking,
    begin_at: beginAt.toDate(),
    end_at
  };
};

exports.syncTimeWindow = syncTimeWindow;

const syncPeerBookings = async (booking, db) => {
  const bookingsService = bookingsServiceFactory(db);
  const peerBookings = await bookingsService.find({
    destination: booking.destination.id,
    active: true
  });

  if (!peerBookings.length) {
    return [];
  }

  const sortedPeerBookings = peerBookings.sort((a, b) => new Date(a.begin_at) - new Date(b.begin_at));
  return Promise.all(sortedPeerBookings.map(async (peerBooking, i) => {
    const now = (0, _moment.default)(new Date());
    const autoBeginAt = (0, _moment.default)(peerBooking.created_at).add(_timing.DEFAULT_BOOKING_COMMENCEMENT_PERIOD_MINUTES + i * _timing.DEFAULT_BOOKING_PERIOD_MINUTES, 'minutes');
    const earliestBeginAt = (0, _moment.default)(peerBooking.created_at).add(_timing.DEFAULT_BOOKING_COMMENCEMENT_PERIOD_MINUTES);

    const beginAt = _moment.default.max(now, _moment.default.max(autoBeginAt, earliestBeginAt)).toDate();

    const updatedPeerBooking = await bookingsService.update(peerBooking.id, {
      begin_at: beginAt
    });
    return {
      peerBooking,
      updatedPeerBooking
    };
  }));
};

exports.syncPeerBookings = syncPeerBookings;
var _default = {
  canTransitionToStatus,
  syncTimeWindow,
  syncPeerBookings
};
exports.default = _default;