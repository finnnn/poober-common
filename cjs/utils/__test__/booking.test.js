"use strict";

var _bookingStatuses = require("../../constants/bookingStatuses");

require('../test');

const tap = require('tap');

const moment = require('moment');

const {
  syncTimeWindow,
  canTransitionToStatus
} = require('../booking');

tap.test('utils/booking', t => {
  t.test('syncTimeWindow()', t => {
    t.plan(2);
    const now = moment();
    const result = syncTimeWindow({
      begin_at: now.clone().subtract(10, 'minutes').toDate(),
      end_at: now.clone().add(20, 'minutes').toDate()
    }, now.toDate());
    t.equalDate(result.begin_at, now.clone().add(10, 'minutes').toDate());
    t.equalDate(result.end_at, now.clone().add(30, 'minutes').toDate());
  });
  t.test('canTransitionToStatus()', t => {
    t.plan(16);
    const idPopeye = 1;
    const idOlive = 2;
    t.ok(canTransitionToStatus({
      booking: {
        requester: {
          id: idPopeye
        },
        destination: {
          owner: {
            id: idOlive
          }
        },
        status: _bookingStatuses.APPROVAL_PENDING
      },
      status: _bookingStatuses.COMMENCEMENT_PENDING,
      userId: idOlive
    }));
    t.ok(canTransitionToStatus({
      booking: {
        requester: {
          id: idPopeye
        },
        destination: {
          owner: {
            id: idOlive
          }
        },
        status: _bookingStatuses.APPROVAL_PENDING
      },
      status: _bookingStatuses.CANCELLED,
      userId: idPopeye
    }));
    t.notOk(canTransitionToStatus({
      booking: {
        requester: {
          id: idPopeye
        },
        destination: {
          owner: {
            id: idOlive
          }
        },
        status: _bookingStatuses.APPROVAL_PENDING
      },
      status: _bookingStatuses.CANCELLED,
      userId: idOlive
    }));
    t.notOk(canTransitionToStatus({
      booking: {
        requester: {
          id: idPopeye
        },
        destination: {
          owner: {
            id: idOlive
          }
        },
        status: _bookingStatuses.COMMENCEMENT_SUCCESSFUL
      },
      status: _bookingStatuses.CANCELLED,
      userId: idPopeye
    }));
    t.ok(canTransitionToStatus({
      booking: {
        requester: {
          id: idPopeye
        },
        destination: {
          owner: {
            id: idOlive
          }
        },
        status: _bookingStatuses.APPROVAL_PENDING
      },
      status: _bookingStatuses.APPROVAL_DECLINED,
      userId: idOlive
    }));
    t.ok(canTransitionToStatus({
      booking: {
        requester: {
          id: idPopeye
        },
        destination: {
          owner: {
            id: idOlive
          }
        },
        status: _bookingStatuses.COMMENCEMENT_PENDING
      },
      status: _bookingStatuses.COMMENCEMENT_SUCCESSFUL,
      userId: idPopeye
    }));
    t.ok(canTransitionToStatus({
      booking: {
        requester: {
          id: idPopeye
        },
        destination: {
          owner: {
            id: idOlive
          }
        },
        status: _bookingStatuses.COMMENCEMENT_PENDING
      },
      status: _bookingStatuses.COMMENCEMENT_PROBLEMATIC,
      userId: idPopeye
    }));
    t.ok(canTransitionToStatus({
      booking: {
        requester: {
          id: idPopeye
        },
        destination: {
          owner: {
            id: idOlive
          }
        },
        status: _bookingStatuses.COMMENCEMENT_SUCCESSFUL
      },
      status: _bookingStatuses.EXECUTION_SUCCESSFUL,
      userId: idPopeye
    }));
    t.notOk(canTransitionToStatus({
      booking: {
        requester: {
          id: idPopeye
        },
        destination: {
          owner: {
            id: idOlive
          }
        },
        status: _bookingStatuses.APPROVAL_PENDING
      },
      status: _bookingStatuses.EXECUTION_SUCCESSFUL,
      userId: idOlive
    }));
    t.notOk(canTransitionToStatus({
      booking: {
        requester: {
          id: idPopeye
        },
        destination: {
          owner: {
            id: idOlive
          }
        },
        status: _bookingStatuses.APPROVAL_PENDING
      },
      status: _bookingStatuses.COMMENCEMENT_PENDING,
      userId: idPopeye
    }));
    t.notOk(canTransitionToStatus({
      booking: {
        requester: {
          id: idPopeye
        },
        destination: {
          owner: {
            id: idOlive
          }
        },
        status: _bookingStatuses.APPROVAL_DECLINED
      },
      status: _bookingStatuses.COMMENCEMENT_PENDING,
      userId: idPopeye
    }));
    t.notOk(canTransitionToStatus({
      booking: {
        requester: {
          id: idPopeye
        },
        destination: {
          owner: {
            id: idOlive
          }
        },
        status: _bookingStatuses.APPROVAL_EXPIRED
      },
      status: _bookingStatuses.COMMENCEMENT_PENDING,
      userId: idPopeye
    }));
    t.notOk(canTransitionToStatus({
      booking: {
        requester: {
          id: idPopeye
        },
        destination: {
          owner: {
            id: idOlive
          }
        },
        status: _bookingStatuses.COMMENCEMENT_PENDING
      },
      status: _bookingStatuses.EXECUTION_SUCCESSFUL,
      userId: idPopeye
    }));
    t.notOk(canTransitionToStatus({
      booking: {
        requester: {
          id: idPopeye
        },
        destination: {
          owner: {
            id: idOlive
          }
        },
        status: _bookingStatuses.COMMENCEMENT_PENDING
      },
      status: _bookingStatuses.COMMENCEMENT_SUCCESSFUL,
      userId: idOlive
    }));
    t.notOk(canTransitionToStatus({
      booking: {
        requester: {
          id: idPopeye
        },
        destination: {
          owner: {
            id: idOlive
          }
        },
        status: _bookingStatuses.COMMENCEMENT_SUCCESSFUL
      },
      status: _bookingStatuses.EXECUTION_SUCCESSFUL,
      userId: idOlive
    }));
    t.notOk(canTransitionToStatus({
      booking: {
        requester: {
          id: idPopeye
        },
        destination: {
          owner: {
            id: idOlive
          }
        },
        status: _bookingStatuses.COMMENCEMENT_SUCCESSFUL
      },
      status: _bookingStatuses.COMMENCEMENT_PROBLEMATIC,
      userId: idPopeye
    }));
  });
  t.end();
});