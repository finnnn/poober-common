"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.close = exports.publish = exports.subscribe = exports.init = void 0;

const ascoltatori = require('ascoltatori');

const EVENT_PRIORITIES = require('../constants/eventPriorities');

let bus; // const handlers = [];

const init = () => new Promise((resolve, reject) => {
  const settings = {
    type: 'mongo',
    url: process.env.POOBER_MONGO_URL,
    pubsubCollection: 'mq'
  };
  ascoltatori.build(settings, (err, ascoltatore) => {
    if (err) {
      reject(err);
      return;
    }

    bus = ascoltatore;
    resolve(bus);
  });
});

exports.init = init;

const subscribe = handler => {
  bus.subscribe(EVENT_PRIORITIES.DEFAULT, (priority, message) => {
    handler({
      priority,
      message: JSON.parse(message)
    });
  });
};

exports.subscribe = subscribe;

const publish = ({
  type,
  payload
}) => {
  bus.publish(EVENT_PRIORITIES.DEFAULT, JSON.stringify({
    type,
    payload
  }), () => {});
}; // export const unsubscribe = async (topic) =>
//   Promise.all((handlers[topic] || []).map(handler => new Promise(resolve => {
//     bus.unsubscribe(topic, handler, resolve);
//   })));


exports.publish = publish;

const close = async () => new Promise(resolve => {
  bus.close(() => {
    console.log('closed mq');
    resolve();
  });
});

exports.close = close;