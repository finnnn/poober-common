"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.validateEmail = exports.validatePhoneNumber = void 0;

var _boom = _interopRequireDefault(require("./boom"));

var _users = _interopRequireDefault(require("../services/users"));

var _db = _interopRequireDefault(require("./db"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const usersService = (0, _users.default)(_db.default);
const VALIDATION_PERIOD_MINUTES = 10;

const validatePhoneNumber = async (id, validationCode) => {
  const user = await usersService.get(id);

  if (!user) {
    return false;
  }

  if (user.phone_validated) {
    return true;
  }

  if (moment(user.phone_validation_code_created_at).add(VALIDATION_PERIOD_MINUTES, 'minutes').isAfter(moment())) {
    throw _boom.default.tooManyRequests('validation period expired');
  }

  if (user.phone_validation_code === validationCode) {
    await usersService.update(id, {
      phone_validated: true
    });
    return true;
  }

  return false;
};

exports.validatePhoneNumber = validatePhoneNumber;

const validateEmail = async (id, validationCode) => {
  const user = await usersService.get(id);

  if (!user) {
    return false;
  }

  if (user.email_validated) {
    return true;
  }

  if (moment(user.email_validation_code_created_at).add(VALIDATION_PERIOD_MINUTES, 'minutes').isAfter(moment())) {
    throw _boom.default.tooManyRequests('validation period expired');
  }

  if (user.email_validation_code === validationCode) {
    await usersService.update(id, {
      email_validated: true
    });
    return true;
  }

  return false;
};

exports.validateEmail = validateEmail;
var _default = {
  validatePhoneNumber,
  validateEmail
};
exports.default = _default;