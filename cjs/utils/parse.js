"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.int = void 0;

var _lodash = require("lodash");

const int = value => {
  const result = parseInt(value);
  return (0, _lodash.isNaN)(result) ? null : result;
};

exports.int = int;