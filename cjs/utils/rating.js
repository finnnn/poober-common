"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.calculateRating = void 0;

const calculateRating = ({
  rating = 3,
  ratingCount = 1
}) => Math.round(rating / ratingCount);

exports.calculateRating = calculateRating;