"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.normalizePhoneNumber = exports.validatePhoneNumber = void 0;

var _phone = _interopRequireDefault(require("phone"));

var _countryCodes = _interopRequireDefault(require("countrycodes/countryCodes"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const validatePhoneNumber = (phoneNumber, countryCode) => {
  if (!phoneNumber || !countryCode) return false;
  const [normalisedPhoneNumber] = (0, _phone.default)(phoneNumber.replace('+', ''), _countryCodes.default.getISO3(countryCode));
  if (!normalisedPhoneNumber) return false;
  return true;
};

exports.validatePhoneNumber = validatePhoneNumber;

const normalizePhoneNumber = number => {
  const [phoneNumber, countryCode] = (0, _phone.default)(number);
  return phoneNumber ? {
    phoneNumber,
    countryCode
  } : null;
};

exports.normalizePhoneNumber = normalizePhoneNumber;