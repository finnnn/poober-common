"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.tooManyRequests = exports.notFound = void 0;
// made this because boom wasnt working on react native
const STATUS_CODES = {
  notFound: 404,
  tooManyRequests: 429
};
const STATUS_TEXT = {
  notFound: 'Not Found',
  tooManyRequests: 'Too Many Requests'
};

const notFound = message => {
  const result = new Error(message);
  result.isBoom = true;
  result.output = {
    statusCode: STATUS_CODES.notFound,
    payload: {
      statusCode: STATUS_CODES.notFound,
      error: STATUS_TEXT.notFound,
      message
    }
  };
  return result;
};

exports.notFound = notFound;

const tooManyRequests = message => {
  const result = new Error(message);
  result.isBoom = true;
  result.output = {
    statusCode: STATUS_CODES.tooManyRequests,
    payload: {
      statusCode: STATUS_CODES.tooManyRequests,
      error: STATUS_TEXT.tooManyRequests,
      message
    }
  };
  return result;
};

exports.tooManyRequests = tooManyRequests;
var _default = {
  notFound,
  tooManyRequests
};
exports.default = _default;