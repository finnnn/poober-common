"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

const knex = require('knex');

const knexPostgis = require('knex-postgis');

const db = knex({
  client: 'pg',
  dialect: 'postgres',
  searchPath: ['knex', 'public', 'postgis'],
  connection: process.env.POOBER_DB_URL
});
knexPostgis(db);
var _default = db;
exports.default = _default;
module.exports = exports.default;