"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.send = exports.init = void 0;

var _mail = _interopRequireDefault(require("@sendgrid/mail"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const init = apiKey => {
  _mail.default.setApiKey(apiKey);
};

exports.init = init;

const send = ({
  to,
  from,
  subject,
  template,
  templateData
}) => {
  _mail.default.send({
    to,
    from,
    subject,
    html: require(`../emailTemplates/html/${template}`)(templateData),
    text: require(`../emailTemplates/text/${template}`)(templateData)
  });
}; // // using SendGrid's v3 Node.js Library
// // https://github.com/sendgrid/sendgrid-nodejs
// const msg = {
//   to: 'test@example.com',
//   from: 'test@example.com',
//   subject: 'Sending with SendGrid is Fun',
//   text: 'and easy to do anywhere, even with Node.js',
//   html: '<strong>and easy to do anywhere, even with Node.js</strong>',
// };


exports.send = send;