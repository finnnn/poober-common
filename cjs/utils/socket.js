"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.userRoomKey = void 0;

const userRoomKey = user => `USER_${user.id}`;

exports.userRoomKey = userRoomKey;