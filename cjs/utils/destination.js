"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.book = exports.isAvailable = void 0;

var _moment = _interopRequireDefault(require("moment"));

var _businessHours = _interopRequireDefault(require("../services/businessHours"));

var _bookings = _interopRequireDefault(require("../services/bookings"));

var _destinations = _interopRequireDefault(require("../services/destinations"));

var _db = _interopRequireDefault(require("../utils/db"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const boom = require('boom');

const {
  COMMENCEMENT_PENDING,
  APPROVAL_PENDING
} = require('../constants/bookingStatuses');

const {
  DEFAULT_BOOKING_PERIOD_MINUTES,
  DEFAULT_BOOKING_COMMENCEMENT_PERIOD_MINUTES
} = require('../constants/timing');

const businessHoursService = (0, _businessHours.default)(_db.default);
const bookingsService = (0, _bookings.default)(_db.default);
const destinationsService = (0, _destinations.default)(_db.default);

const isAvailable = async ({
  destination,
  beginAt,
  endAt // timezone,

}) => {
  const beginDay = (0, _moment.default)(beginAt).day();
  const endDay = (0, _moment.default)(endAt).day();
  const beginMinutes = (0, _moment.default)(beginAt).minutes();
  const endMinutes = (0, _moment.default)(endAt).minutes();

  if (beginDay === endDay) {
    const businessHours = await businessHoursService.find({
      query: {
        entity_type: 'destination',
        entity_id: destination,
        // timezone: ,
        day: beginDay
      }
    });
    if (!businessHours.length) return new Error('the destination has no business hours');
    const businessHoursThatSpanTheDesiredPeriod = businessHours.filter(hours => hours.opening_time <= beginMinutes && hours.closing_time >= endMinutes);
    if (!businessHoursThatSpanTheDesiredPeriod.length) return new Error('the destination is not available for this period');
    const bookings = await bookingsService.find({
      destination,
      begin_day: beginDay,
      active: true
    });
    const bookingsThatSpanTheDesiredPeriod = bookings.filter(booking => booking.begin_at <= endAt && booking.begin_at >= beginAt || booking.end_at <= endAt && booking.end_at >= beginAt);
    if (bookingsThatSpanTheDesiredPeriod.length) return new Error('the destination is booked during this period');
  } else {
    const beginDayBusinessHours = await businessHoursService.find({
      query: {
        entity_type: 'destination',
        entity_id: destination,
        // timezone: ,
        day: beginDay
      }
    });
    const endDayBusinessHours = await businessHoursService.find({
      query: {
        entity_type: 'destination',
        entity_id: destination,
        // timezone: ,
        day: endDay
      }
    });
    if (!beginDayBusinessHours.length || !endDayBusinessHours.length) return new Error('the destination is not available for this period');
    const businessHoursThatSpanTheDesiredPeriodOnBeginDay = beginDayBusinessHours.filter(hours => hours.opening_time <= beginMinutes && hours.closing_time === 60 * 24);
    const businessHoursThatSpanTheDesiredPeriodOnEndDay = endDayBusinessHours.filter(hours => hours.opening_time === 0 && hours.closing_time >= endMinutes);
    if (!businessHoursThatSpanTheDesiredPeriodOnBeginDay.length || !businessHoursThatSpanTheDesiredPeriodOnEndDay.length) return new Error('the destination is not available for this period');
    const beginDayBookings = await bookingsService.find({
      destination,
      begin_day: beginDay,
      active: true
    });
    const endDayBookings = await bookingsService.find({
      destination,
      begin_day: endDay,
      active: true
    });
    const bookingsThatSpanTheDesiredPeriod = beginDayBookings.concat(endDayBookings).filter(booking => booking.begin_at <= endAt && booking.begin_at >= beginAt || booking.end_at <= endAt && booking.end_at >= beginAt);
    if (!bookingsThatSpanTheDesiredPeriod.length) return new Error('the destination is booked during this period');
  }

  return true;
};

exports.isAvailable = isAvailable;

const book = async ({
  destinationId,
  userId
}) => {
  const destination = await destinationsService.get(destinationId);
  const activeBookingsForUser = await bookingsService.find({
    requester: userId,
    active: true
  });

  if (activeBookingsForUser.length) {
    throw boom.badRequest('you already have an active booking');
  }

  if (destination.owner.id === userId) {
    throw boom.badRequest('you are attempting to book your own venue');
  } // try { 
  //   await isAvailable({
  //     destination: destinationId,
  //     // beginAt: req.body.begin_at,
  //     // endAt: req.body.end_at,
  //     // timezone,
  //   });
  // } catch (error) {
  //   return throw boom.badRequest(error.message);
  // }


  const activeBookings = await bookingsService.find({
    destination: destinationId,
    active: true
  });
  let status;

  if (destination.auto_accept_bookings) {
    status = COMMENCEMENT_PENDING;
  } else {
    status = APPROVAL_PENDING;
  }

  const sortedActiveBookings = activeBookings.sort((a, b) => new Date(a.end_at) - new Date(b.end_at));
  const isVacant = !sortedActiveBookings.length;
  let booking;

  if (isVacant) {
    const now = new Date();
    booking = await bookingsService.create({
      destination: destinationId,
      requester: userId,
      begin_at: (0, _moment.default)(now).add(DEFAULT_BOOKING_COMMENCEMENT_PERIOD_MINUTES, 'minutes'),
      end_at: (0, _moment.default)(now).add(DEFAULT_BOOKING_PERIOD_MINUTES + DEFAULT_BOOKING_COMMENCEMENT_PERIOD_MINUTES, 'minutes').toDate(),
      status,
      active: true
    });
  } else {
    const soonestAvailability = sortedActiveBookings[sortedActiveBookings.length - 1].end_at;
    const prescribedBeginAt = (0, _moment.default)().add(DEFAULT_BOOKING_COMMENCEMENT_PERIOD_MINUTES, 'minutes').toDate();
    const beginAt = (0, _moment.default)(soonestAvailability).isBefore(prescribedBeginAt) ? prescribedBeginAt : soonestAvailability;
    booking = await bookingsService.create({
      destination: destinationId,
      requester: userId,
      begin_at: soonestAvailability,
      end_at: (0, _moment.default)(beginAt).add(DEFAULT_BOOKING_PERIOD_MINUTES, 'minutes').toDate(),
      status,
      active: true
    });
  }

  return booking;
};

exports.book = book;