"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.jsonToPoint = exports.pointToJson = void 0;
const POINT_MAPPING = ['latitude', 'longitude'];

const pointToJson = point => point.replace('POINT(', '').replace(')', '').split(' ').reduce((acc, val, i) => ({ ...acc,
  [POINT_MAPPING[i]]: parseFloat(val)
}), {});

exports.pointToJson = pointToJson;

const jsonToPoint = args => {
  const {
    db,
    point: {
      lat,
      lon
    }
  } = args;
  return db.postgis.geomFromText(`Point(${lat} ${lon})`, 4326);
};

exports.jsonToPoint = jsonToPoint;
var _default = {
  pointToJson,
  jsonToPoint
};
exports.default = _default;