"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _lodash = require("lodash");

var _actions = _interopRequireDefault(require("../constants/actions"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _default = (state = [], action) => {
  switch (action.type) {
    case _actions.default.bookings.BOOKING_CREATE_SUCCEEDED:
      {
        return state.concat(action.payload);
      }

    case _actions.default.bookings.BOOKINGS_LIST_SUCCEEDED:
      {
        return (0, _lodash.uniqBy)(state.concat(action.payload), 'id');
      }

    case _actions.default.bookings.BOOKING_UPDATE_SUCCEEDED:
      {
        const index = (0, _lodash.findIndex)(state, {
          id: action.payload.id
        });
        const nextState = [...state];
        nextState[index] = action.payload;
        return nextState;
      }

    case _actions.default.bookings.BOOKING_READ_SUCCEEDED:
      {
        const index = (0, _lodash.findIndex)(state, {
          id: action.payload.id
        });
        const nextState = [...state];

        if (index !== -1) {
          nextState[index] = action.payload;
        } else {
          nextState.push(action.payload);
        }

        return nextState;
      }

    case _actions.default.bookings.BOOKING_USER_LOCATION_UPDATED:
      {
        const index = (0, _lodash.findIndex)(state, {
          id: action.payload.bookingId
        });
        const nextState = [...state];
        nextState[index].userLocation = action.payload.location;
        return nextState;
      }

    case _actions.default.bookings.BOOKING_REMOVE_SUCCEEDED:
      {
        const index = (0, _lodash.findIndex)(state, {
          id: action.payload.id
        });
        return [...state.slice(0, index), ...state.slice(index + 1)];
      }

    case 'CLEAR_DATA':
      {
        return [];
      }

    default:
      {
        return state;
      }
  }
};

exports.default = _default;
module.exports = exports.default;