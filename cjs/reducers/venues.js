"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

const {
  findIndex,
  uniqBy
} = require('lodash');

const actions = require('../constants/actions');

var _default = (state = [], action) => {
  switch (action.type) {
    case actions.venues.VENUE_CREATE_SUCCEEDED:
      {
        return state.concat(action.payload);
      }

    case actions.venues.VENUE_UPDATE_SUCCEEDED:
      {
        const existing = state.filter(({
          id
        }) => id === action.payload.id);
        Object.assign(existing, action.payload);
        return state.slice();
      }

    case actions.venues.VENUES_LIST_SUCCEEDED:
      {
        return uniqBy(state.concat(action.payload), 'id');
      }

    case actions.venues.VENUE_REMOVE_SUCCEEDED:
      {
        const index = findIndex(state, {
          id: action.payload.id
        });
        return [...state.slice(0, index), ...state.slice(index + 1)];
      }

    case 'CLEAR_DATA':
      {
        return [];
      }

    default:
      {
        return state;
      }
  }
};

exports.default = _default;
module.exports = exports.default;