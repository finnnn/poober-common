"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _redux = require("redux");

var _auth = _interopRequireDefault(require("../reducers/auth"));

var _destinations = _interopRequireDefault(require("../reducers/destinations"));

var _bookings = _interopRequireDefault(require("../reducers/bookings"));

var _venues = _interopRequireDefault(require("../reducers/venues"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _default = (0, _redux.combineReducers)({
  auth: _auth.default,
  destinations: _destinations.default,
  bookings: _bookings.default,
  venues: _venues.default
});

exports.default = _default;
module.exports = exports.default;