"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _lodash = require("lodash");

var _actions = _interopRequireDefault(require("../constants/actions"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _default = (state = {}, action) => {
  const {
    type,
    payload
  } = action;

  switch (type) {
    case _actions.default.chat.CHAT_MESSAGE_CREATE_SUCCEEDED:
      {
        const nextState = { ...state
        };
        const otherPartyId = payload.outbound ? payload.recipient.id : payload.sender.id;
        nextState[otherPartyId] = nextState[otherPartyId] || [];
        nextState[otherPartyId].push(payload);
        return nextState;
      }

    case _actions.default.chat.CHAT_MESSAGES_LIST_SUCCEEDED:
      {
        const nextState = { ...state
        };
        nextState[payload.recipient] = nextState[payload.recipient] || [];
        nextState[payload.recipient] = (0, _lodash.uniqBy)(nextState[payload.recipient].concat(payload.messages), 'id');
        return nextState;
      }

    case _actions.default.chat.CHAT_MESSAGE_UPDATE_SUCCEEDED:
      {
        const nextState = { ...state
        };
        const otherPartyId = payload.outbound ? payload.recipient : payload.sender;
        nextState[otherPartyId] = nextState[otherPartyId] || [];
        const index = (0, _lodash.findIndex)(nextState[otherPartyId], {
          id: action.payload.message.id
        });
        nextState[otherPartyId] = nextState[otherPartyId][index] = { ...nextState[otherPartyId][index],
          ...payload.message
        };
        return nextState;
      }

    case _actions.default.chat.CHAT_MESSAGE_REMOVE_SUCCEEDED:
      {
        const nextState = { ...state
        };
        nextState[payload.recipient] = nextState[payload.recipient] || [];
        const index = (0, _lodash.findIndex)(nextState[payload.recipient], {
          id: action.payload.message.id
        });
        nextState[payload.recipient] = nextState[payload.recipient] = [...nextState[payload.recipient].slice(0, index), ...nextState[payload.recipient].slice(index + 1)];
        return nextState;
      }

    default:
      {
        return state;
      }
  }
};

exports.default = _default;
module.exports = exports.default;