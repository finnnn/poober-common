"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _lodash = require("lodash");

const actions = require('../constants/actions');

var _default = (state = [], action) => {
  switch (action.type) {
    case actions.destinations.DESTINATION_CREATE_SUCCEEDED:
      {
        return state.concat(action.payload);
      }

    case actions.destinations.DESTINATIONS_LIST_SUCCEEDED:
      {
        return (0, _lodash.uniqBy)(state.concat(action.payload), 'id');
      }

    case actions.destinations.DESTINATION_UPDATE_SUCCEEDED:
      {
        const index = (0, _lodash.findIndex)(state, {
          id: action.payload.id
        });
        const nextState = [...state];
        nextState[index] = action.payload;
        return nextState;
      }

    case actions.destinations.DESTINATION_REMOVE_SUCCEEDED:
      {
        const index = (0, _lodash.findIndex)(state, {
          id: action.payload.id
        });
        return [...state.slice(0, index), ...state.slice(index + 1)];
      }

    case 'CLEAR_DATA':
      {
        return [];
      }

    default:
      {
        return state;
      }
  }
};

exports.default = _default;
module.exports = exports.default;