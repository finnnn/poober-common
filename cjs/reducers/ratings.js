"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _lodash = require("lodash");

const actions = require('../constants/actions');

const DEFAULT_STATE = {
  user: {},
  destination: {}
};

var _default = (state = DEFAULT_STATE, action) => {
  switch (action.type) {
    case actions.ratings.RATING_CREATE_SUCCEEDED:
      {
        const nextState = { ...state
        };
        const {
          entity_type,
          entity_id,
          rating,
          review,
          rater,
          id
        } = action.payload;
        nextState[entity_type][entity_id] = nextState[entity_type][entity_id] || [];
        nextState[entity_type][entity_id].concat({
          rating,
          review,
          rater,
          id
        });
        return nextState;
      }

    case actions.ratings.RATINGS_LIST_SUCCEEDED:
      {
        const nextState = { ...state
        };
        action.payload.forEach(ratingItem => {
          const {
            entity_type,
            entity_id,
            rating,
            review,
            rater,
            id
          } = ratingItem;
          nextState[entity_type][entity_id] = nextState[entity_type][entity_id] || [];
          (0, _lodash.uniqBy)(nextState[entity_type][entity_id].concat({
            rating,
            review,
            rater,
            id
          }), 'id');
        });
        return nextState;
      }

    case actions.ratings.RATING_UPDATE_SUCCEEDED:
      {
        const nextState = { ...state
        };
        const {
          entity_type,
          entity_id,
          rating,
          review,
          rater,
          id
        } = action.payload;
        const index = (0, _lodash.findIndex)(nextState[entity_type][entity_id], {
          id
        });
        nextState[entity_type][entity_id][index] = {
          rating,
          review,
          rater,
          id
        };
        return nextState;
      }

    case actions.ratings.RATING_REMOVE_SUCCEEDED:
      {
        const nextState = { ...state
        };
        const {
          entity_type,
          entity_id,
          id
        } = action.payload;
        const index = (0, _lodash.findIndex)(nextState[entity_type][entity_id], {
          id
        });
        nextState[entity_type][entity_id] = [...nextState[entity_type][entity_id].slice(0, index), ...nextState[entity_type][entity_id].slice(index + 1)];
        return nextState;
      }

    case 'CLEAR_DATA':
      {
        return DEFAULT_STATE;
      }

    default:
      {
        return state;
      }
  }
};

exports.default = _default;
module.exports = exports.default;