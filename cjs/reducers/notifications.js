"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _lodash = require("lodash");

const actions = require('../constants/actions');

const filter = state => (0, _lodash.uniqBy)(state, 'id');

const sort = state => (0, _lodash.sortBy)(state, 'created_at');

const replace = (collection, item) => {
  const index = (0, _lodash.findIndex)(collection, {
    id: item.id
  });
  const nextCollection = [...collection];
  nextCollection[index] = item;
  return nextCollection;
};

var _default = (state = [], action) => {
  switch (action.type) {
    case actions.notifications.NOTIFICATION_CREATE_SUCCEEDED:
      {
        return sort(filter(state.concat(action.payload)));
      }

    case actions.notifications.NOTIFICATIONS_LIST_SUCCEEDED:
      {
        return sort(filter(state.concat(action.payload)));
      }

    case actions.notifications.NOTIFICATION_UPDATE_SUCCEEDED:
      {
        if ((0, _lodash.isArray)(action.payload)) {
          const nextState = action.payload.reduce((acc, item) => replace(acc, item), state);
          return sort(filter(nextState));
        }

        return sort(filter(replace(state, action.payload)));
      }

    case 'CLEAR_DATA':
      {
        return [];
      }

    default:
      {
        return state;
      }
  }
};

exports.default = _default;
module.exports = exports.default;