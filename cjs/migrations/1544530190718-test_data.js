"use strict";

var _nanoid = _interopRequireDefault(require("nanoid"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const moment = require('moment');

const bcrypt = require('bcrypt');

const db = require('../utils/db');

const gis = require('../utils/gis');

const TABLES = require('../constants/tables');

const alphabet = 'abcdefghijklmnopqrstuvwxyz'.split('');
const SALT_ROUNDS = 10;

const randomize = val => val + Math.random() * 0.01 * (Math.random() > 0.5 ? 1 : -1);

const randomPoint = (startLatitude = 42.68, startLongitude = 23.32) => ({
  lat: randomize(startLatitude),
  lon: randomize(startLongitude)
});

const TEST_USERS = [{
  name: 'popeye',
  email: 'popeye@poober.com',
  phone_number: '+359884155667',
  phone_country: 'BGR',
  password: bcrypt.hashSync('P', SALT_ROUNDS)
}, {
  name: 'olive',
  email: 'olive@poober.com',
  phone_number: '+359884155668',
  phone_country: 'BGR',
  password: bcrypt.hashSync('P', SALT_ROUNDS)
}];

module.exports.up = async function (next) {
  if (process.env.POOBER_TEST === 'yes') {
    next();
    return;
  }

  const created_at = new Date();
  const testUsers = await db(TABLES.USERS).returning('*').insert(TEST_USERS.map(user => ({ ...user,
    created_at,
    updated_at: created_at,
    phone_validated: true,
    phone_validation_code: (0, _nanoid.default)(),
    phone_validation_code_created_at: created_at,
    email_validated: true,
    email_validation_code: (0, _nanoid.default)(),
    email_validation_code_created_at: created_at
  }))).then(r => r);
  const testDestinations = await db(TABLES.DESTINATIONS).returning('*').insert(alphabet.map((letter, i) => ({
    name: `Venue ${letter.toUpperCase()}`,
    location: gis.jsonToPoint({
      db,
      point: randomPoint()
    }),
    owner: testUsers[i % 2].id,
    created_at,
    updated_at: created_at,
    auto_accept_bookings: false
  }))).then(r => r);
  await db(TABLES.BOOKINGS).returning('*').insert(testDestinations.slice(0, testDestinations.length / 2).map((destination, i) => ({
    destination: destination.id,
    begin_at: moment().add(i, 'hours').toDate(),
    end_at: moment().add(i, 'hours').add(15, 'minutes').toDate(),
    created_at,
    updated_at: created_at,
    requester: testUsers.filter(({
      id
    }) => id !== destination.owner)[0].id,
    status: 'APPROVAL_PENDING'
  }))).then(() => {
    setTimeout(next);
  });
};

module.exports.down = function (next) {
  next();
};