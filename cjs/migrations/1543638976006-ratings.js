"use strict";

const db = require('../utils/db');

const {
  createTable,
  dropTable
} = require('../utils/migrate')(db);

const TABLES = require('../constants/tables');

module.exports.up = next => {
  createTable(TABLES.RATINGS, table => {
    if (!table) {
      next();
      return;
    }

    table.increments();
    table.timestamps();
    table.integer('rater');
    table.string('entity_type');
    table.integer('entity_id');
    table.integer('rating');
    table.text('review');
    setTimeout(next);
  });
};

module.exports.down = next => {
  dropTable(TABLES.RATINGS, next);
};