"use strict";

const db = require('../utils/db');

const {
  createTable,
  dropTable
} = require('../utils/migrate')(db);

const TABLES = require('../constants/tables');

module.exports.up = next => {
  createTable(TABLES.NOTIFICATIONS, table => {
    if (table) {
      table.increments();
      table.integer('recipient');
      table.string('type');
      table.string('payload');
      table.boolean('active');
      table.timestamps();
    }

    setTimeout(next);
  });
};

module.exports.down = next => {
  dropTable(TABLES.NOTIFICATIONS, next);
};