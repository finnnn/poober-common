"use strict";

const db = require('../utils/db');

const TABLES = require('../constants/tables');

module.exports.up = next => {
  db.schema.table(TABLES.DESTINATIONS, table => {
    if (!table) {
      next();
      return;
    }

    table.integer('owner');
  }).then(() => {
    setTimeout(next);
  });
};

module.exports.down = next => {
  console.log('down');
  next();
};