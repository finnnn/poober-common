"use strict";

const moment = require('moment');

const db = require('../utils/db');

const TABLES = require('../constants/tables');

module.exports.up = async function (next) {
  if (process.env.POOBER_TEST === 'yes') {
    next();
    return;
  }

  const destinations = await db(TABLES.DESTINATIONS).select().then(d => d);
  const created_at = new Date();

  try {
    await Promise.all(destinations.slice(5).map(destination => {
      const days = [];

      for (let i = 0; i < 7; i++) {
        days.push({
          entity_id: destination.id,
          entity_type: 'destination',
          timezone: moment().format('ZZ'),
          opening_time: 9 * 60,
          // 9am
          closing_time: 17 * 60,
          // 5pm
          day: i,
          created_at,
          updated_at: created_at
        });
      }

      return db(TABLES.BUSINESS_HOURS).insert(days).then(() => {});
    }));
  } catch (error) {
    next(error);
    return;
  }

  setTimeout(next);
};

module.exports.down = function (next) {
  next();
};