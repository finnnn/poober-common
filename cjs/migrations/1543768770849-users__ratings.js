"use strict";

const db = require('../utils/db');

const TABLES = require('../constants/tables');

module.exports.up = next => {
  db.schema.table(TABLES.USERS, table => {
    if (!table) {
      next();
      return;
    }

    table.integer('rating');
    table.integer('rating_count');
  }).then(() => {
    setTimeout(next);
  });
};

module.exports.down = next => {
  console.log('down');
  next();
};