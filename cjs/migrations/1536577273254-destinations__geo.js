"use strict";

const db = require('../utils/db');

const TABLES = require('../constants/tables');

module.exports.up = next => {
  db.schema.raw(`ALTER TABLE ${TABLES.DESTINATIONS} ADD location GEOMETRY(POINT,4326)`).then(() => {
    setTimeout(next);
  });
};

module.exports.down = next => {
  next();
};