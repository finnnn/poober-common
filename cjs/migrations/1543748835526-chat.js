"use strict";

const db = require('../utils/db');

const {
  createTable,
  dropTable
} = require('../utils/migrate')(db);

const TABLES = require('../constants/tables');

module.exports.up = next => {
  createTable(TABLES.CHAT_MESSAGES, table => {
    if (!table) {
      next();
      return;
    }

    table.increments();
    table.timestamps();
    table.text('text');
    table.integer('sender');
    table.integer('recipient');
    setTimeout(next);
  });
};

module.exports.down = next => {
  dropTable(TABLES.CHAT_MESSAGES, next);
};