"use strict";

const db = require('../utils/db');

const TABLES = require('../constants/tables');

module.exports.up = next => {
  db.schema.table(TABLES.BOOKINGS, table => {
    if (!table) {
      next();
      return;
    }

    table.boolean('active');
  }).then(() => {
    setTimeout(next);
  });
};

module.exports.down = next => {
  console.log('down');
  next();
};