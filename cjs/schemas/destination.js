"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.LIST = exports.UPDATE = exports.CREATE = void 0;

var _joi = _interopRequireDefault(require("joi"));

var _point = _interopRequireDefault(require("./point"));

var _businessHours = require("./businessHours");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const CREATE = _joi.default.object({
  name: _joi.default.string().required(),
  location: _point.default,
  auto_accept_bookings: _joi.default.boolean(),
  business_hours: _businessHours.CREATE
});

exports.CREATE = CREATE;

const UPDATE = _joi.default.object({
  name: _joi.default.string().required(),
  location: _point.default,
  auto_accept_bookings: _joi.default.boolean(),
  business_hours: _businessHours.UPDATE
});

exports.UPDATE = UPDATE;

const LIST = _joi.default.object({
  origin: _joi.default.string(),
  begin_at: _joi.default.date(),
  end_at: _joi.default.date(),
  limit: _joi.default.number().integer(),
  timezone: _joi.default.string(),
  owner: _joi.default.number().integer(),
  urgent: _joi.default.boolean()
});

exports.LIST = LIST;
var _default = {
  CREATE,
  UPDATE,
  LIST
};
exports.default = _default;