"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.LIST = exports.REMOVE = exports.CREATE = void 0;

var _joi = _interopRequireDefault(require("joi"));

var _notificationTypes = _interopRequireDefault(require("../constants/notificationTypes"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const CREATE = _joi.default.object({
  notification_type: _joi.default.string().allow(Object.values(_notificationTypes.default))
});

exports.CREATE = CREATE;

const REMOVE = _joi.default.object({});

exports.REMOVE = REMOVE;

const LIST = _joi.default.object({});

exports.LIST = LIST;
var _default = {
  CREATE,
  REMOVE,
  LIST
};
exports.default = _default;