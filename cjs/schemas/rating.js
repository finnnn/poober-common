"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.LIST = exports.UPDATE = exports.CREATE = void 0;

var _joi = _interopRequireDefault(require("joi"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const CREATE = _joi.default.object({
  entity_type: _joi.default.string().required(),
  entity_id: _joi.default.number().integer().required(),
  rating: _joi.default.number().integer().required(),
  review: _joi.default.string()
});

exports.CREATE = CREATE;

const UPDATE = _joi.default.object({
  rating: _joi.default.number().integer().required(),
  review: _joi.default.string()
});

exports.UPDATE = UPDATE;

const LIST = _joi.default.object({
  entity_type: _joi.default.string(),
  entity_id: _joi.default.number().integer(),
  rater: _joi.default.number().integer()
});

exports.LIST = LIST;
var _default = {
  CREATE,
  UPDATE,
  LIST
};
exports.default = _default;