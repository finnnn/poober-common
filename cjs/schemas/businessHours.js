"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.UPDATE = exports.CREATE = void 0;

var _joi = _interopRequireDefault(require("joi"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const CREATE = _joi.default.object().pattern(/[0-6]/, _joi.default.object({
  timezone: _joi.default.string().required(),
  opening_time: _joi.default.number().integer().required(),
  closing_time: _joi.default.number().integer().required()
}));

exports.CREATE = CREATE;

const UPDATE = _joi.default.object().pattern(/[0-6]/, _joi.default.object({
  timezone: _joi.default.string().required(),
  opening_time: _joi.default.number().integer().required(),
  closing_time: _joi.default.number().integer().required()
}));

exports.UPDATE = UPDATE;
var _default = {
  CREATE,
  UPDATE
};
exports.default = _default;