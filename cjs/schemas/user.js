"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.UPDATE = exports.CREATE = void 0;

var _joi = _interopRequireDefault(require("joi"));

var _point = _interopRequireDefault(require("./point"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const CREATE = _joi.default.object({
  email: _joi.default.string().required(),
  password: _joi.default.string().required(),
  name: _joi.default.string(),
  location: _point.default
});

exports.CREATE = CREATE;

const UPDATE = _joi.default.object({
  email: _joi.default.string(),
  password: _joi.default.string(),
  location: _point.default,
  name: _joi.default.string()
});

exports.UPDATE = UPDATE;
var _default = {
  CREATE,
  UPDATE
};
exports.default = _default;