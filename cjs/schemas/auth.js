"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.SIGN_UP = exports.SIGN_IN = void 0;

var _joi = _interopRequireDefault(require("joi"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const SIGN_IN = _joi.default.object({
  phone_number: _joi.default.string(),
  phone_country: _joi.default.string().required(),
  password: _joi.default.string().required(),
  email: _joi.default.string().email({
    minDomainAtoms: 2
  })
});

exports.SIGN_IN = SIGN_IN;

const SIGN_UP = _joi.default.object({
  phone_number: _joi.default.string(),
  phone_country: _joi.default.string().required(),
  password: _joi.default.string().required(),
  email: _joi.default.string().email({
    minDomainAtoms: 2
  })
});

exports.SIGN_UP = SIGN_UP;
var _default = {
  SIGN_IN,
  SIGN_UP
};
exports.default = _default;