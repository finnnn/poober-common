"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.LIST = exports.UPDATE = exports.CREATE = void 0;

var _joi = _interopRequireDefault(require("joi"));

var _bookingStatuses = _interopRequireDefault(require("../constants/bookingStatuses"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const CREATE = _joi.default.object({
  destination: _joi.default.number().integer().required() // begin_at: Joi.date().required(),
  // end_at: Joi.date().required(),

});

exports.CREATE = CREATE;

const UPDATE = _joi.default.object({
  status: _joi.default.string().allow(Object.values(_bookingStatuses.default))
});

exports.UPDATE = UPDATE;

const LIST = _joi.default.object({
  requester: _joi.default.string(),
  owner: _joi.default.string()
});

exports.LIST = LIST;
var _default = {
  CREATE,
  UPDATE,
  LIST
};
exports.default = _default;