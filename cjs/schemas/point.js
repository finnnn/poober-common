"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _joi = _interopRequireDefault(require("joi"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _default = _joi.default.object({
  latitude: _joi.default.number().required(),
  longitude: _joi.default.number().required()
});

exports.default = _default;
module.exports = exports.default;