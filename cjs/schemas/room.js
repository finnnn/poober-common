"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.JOIN = void 0;

var _joi = _interopRequireDefault(require("joi"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const JOIN = _joi.default.object({});

exports.JOIN = JOIN;
var _default = {
  JOIN
};
exports.default = _default;