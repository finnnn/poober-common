"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.LIST = exports.UPDATE = exports.CREATE = void 0;

var _joi = _interopRequireDefault(require("joi"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const CREATE = _joi.default.object({
  text: _joi.default.string().required(),
  recipient: _joi.default.number().integer().required(),
  created_at: _joi.default.date()
});

exports.CREATE = CREATE;

const UPDATE = _joi.default.object({
  text: _joi.default.string().required()
});

exports.UPDATE = UPDATE;
const LIST = {
  recipient: _joi.default.number().integer().required()
};
exports.LIST = LIST;
var _default = {
  CREATE,
  UPDATE,
  LIST
};
exports.default = _default;