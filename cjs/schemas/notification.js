"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.UPDATE = exports.LIST = void 0;

var _joi = _interopRequireDefault(require("joi"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const LIST = _joi.default.object({});

exports.LIST = LIST;

const UPDATE = _joi.default.object({
  active: _joi.default.boolean()
});

exports.UPDATE = UPDATE;
var _default = {
  LIST,
  UPDATE
};
exports.default = _default;