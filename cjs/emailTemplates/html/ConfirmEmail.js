"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _mjmlReact = require("mjml-react");

var _Layout = _interopRequireDefault(require("./common/Layout"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _default = ({
  confirmEmailUrl
}) => _react.default.createElement(_Layout.default, {
  title: "Confirm Email",
  preview: "Preview..."
}, _react.default.createElement(_mjmlReact.MjmlSection, null, _react.default.createElement(_mjmlReact.MjmlColumn, null, _react.default.createElement(_mjmlReact.MjmlButton, {
  padding: "20px",
  backgroundColor: "#346DB7",
  href: confirmEmailUrl
}, "Confirm Email"))));

exports.default = _default;
module.exports = exports.default;