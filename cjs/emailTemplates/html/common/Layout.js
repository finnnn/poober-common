"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _mjmlReact = require("mjml-react");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _default = ({
  children,
  title,
  preview
}) => {
  const {
    html,
    errors
  } = (0, _mjmlReact.render)(_react.default.createElement(_mjmlReact.Mjml, null, _react.default.createElement(_mjmlReact.MjmlHead, null, _react.default.createElement(_mjmlReact.MjmlTitle, null, title), _react.default.createElement(_mjmlReact.MjmlPreview, null, preview)), _react.default.createElement(_mjmlReact.MjmlBody, {
    width: 500
  }, children)), {
    validationLevel: 'soft'
  });
  return html;
};

exports.default = _default;
module.exports = exports.default;