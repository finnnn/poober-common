"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _default = ({
  confirmEmailUrl
}) => `<a href="${confirmEmailUrl}">Confirm email</a>`;

exports.default = _default;
module.exports = exports.default;