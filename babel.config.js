module.exports = {
  "presets": [
    [
      "@babel/env",
      {
        "targets": {
          "node": true,
        },
        "modules": "commonjs",
      },
    ],
    "@babel/preset-react",
    // "@babel/preset-stage-2",
  ],
  "plugins": [
    "babel-plugin-add-module-exports",
    // "babel-plugin-transform-class-properties",
    // "babel-plugin-transform-es2015-for-of",
  ]
  .map(require.resolve),
};
