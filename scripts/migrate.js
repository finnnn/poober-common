const db = require('../cjs/utils/db');
const migrate = require('../cjs/utils/migrate')(db); 

migrate.up().then(() => {
  process.exit(0);
});
