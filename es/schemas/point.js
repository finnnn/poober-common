import Joi from 'joi';

export default Joi.object({
  latitude: Joi.number().required(),
  longitude: Joi.number().required(),
});
