import Joi from 'joi';

export const LIST = Joi.object({});

export const UPDATE = Joi.object({
  active: Joi.boolean(),
});

export default {
  LIST,
  UPDATE,
};
