import Joi from 'joi';

export const CREATE = Joi.object()
  .pattern(
    /[0-6]/, Joi.object({
      timezone: Joi.string().required(),
      opening_time: Joi.number().integer().required(),
      closing_time: Joi.number().integer().required(),
    })
  );

export const UPDATE = Joi.object()
  .pattern(
    /[0-6]/, Joi.object({
      timezone: Joi.string().required(),
      opening_time: Joi.number().integer().required(),
      closing_time: Joi.number().integer().required(),
    })
  );

export default {
  CREATE,
  UPDATE,
};
