import Joi from 'joi';
import BOOKING_STATUSES from '../constants/bookingStatuses';

export const CREATE = Joi.object({
  destination: Joi.number().integer().required(),
  // begin_at: Joi.date().required(),
  // end_at: Joi.date().required(),
});

export const UPDATE = Joi.object({
  status: Joi.string().allow(Object.values(BOOKING_STATUSES)),
});

export const LIST = Joi.object({
  requester: Joi.string(),
  owner: Joi.string(),
});

export default {
  CREATE,
  UPDATE,
  LIST,
};
