import Joi from 'joi';

import POINT_SCHEMA from './point';
import {
  CREATE as BUSINESS_HOURS_CREATE_SCHEMA,
  UPDATE as BUSINESS_HOURS_UPDATE_SCHEMA,
} from './businessHours';

export const CREATE = Joi.object({
  name: Joi.string().required(),
  location: POINT_SCHEMA,
  auto_accept_bookings: Joi.boolean(),
  business_hours: BUSINESS_HOURS_CREATE_SCHEMA,
});

export const UPDATE = Joi.object({
  name: Joi.string().required(),
  location: POINT_SCHEMA,
  auto_accept_bookings: Joi.boolean(),
  business_hours: BUSINESS_HOURS_UPDATE_SCHEMA,
});

export const LIST = Joi.object({
  origin: Joi.string(),
  begin_at: Joi.date(),
  end_at: Joi.date(),
  limit: Joi.number().integer(),
  timezone: Joi.string(),
  owner: Joi.number().integer(),
  urgent: Joi.boolean(),
});

export default {
  CREATE,
  UPDATE,
  LIST,
};
