import Joi from 'joi';

import POINT_SCHEMA from './point';

export const CREATE = Joi.object({
  email: Joi.string().required(),
  password: Joi.string().required(),
  name: Joi.string(),
  location: POINT_SCHEMA,
});

export const UPDATE = Joi.object({
  email: Joi.string(),
  password: Joi.string(),
  location: POINT_SCHEMA,
  name: Joi.string(),
});

export default {
  CREATE,
  UPDATE,
};
