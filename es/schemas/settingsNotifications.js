import Joi from 'joi';

import NOTIFICATION_TYPES from '../constants/notificationTypes';

export const CREATE = Joi.object({
  notification_type: Joi.string().allow(Object.values(NOTIFICATION_TYPES)),
});

export const REMOVE = Joi.object({});

export const LIST = Joi.object({});

export default {
  CREATE,
  REMOVE,
  LIST,
};
