import Joi from 'joi';

export const SIGN_IN = Joi.object({
  phone_number: Joi.string(),
  phone_country: Joi.string().required(),
  password: Joi.string().required(),
  email: Joi.string().email({ minDomainAtoms: 2 }),
});

export const SIGN_UP = Joi.object({
  phone_number: Joi.string(),
  phone_country: Joi.string().required(),
  password: Joi.string().required(),
  email: Joi.string().email({ minDomainAtoms: 2 }),
});

export default {
  SIGN_IN,
  SIGN_UP,
};
