import Joi from 'joi';

export const CREATE = Joi.object({
  entity_type: Joi.string().required(),
  entity_id: Joi.number().integer().required(),
  rating: Joi.number().integer().required(),
  review: Joi.string(),
});

export const UPDATE = Joi.object({
  rating: Joi.number().integer().required(),
  review: Joi.string(),
});

export const LIST = Joi.object({
  entity_type: Joi.string(),
  entity_id: Joi.number().integer(),
  rater: Joi.number().integer(),
});

export default {
  CREATE,
  UPDATE,
  LIST,
};
