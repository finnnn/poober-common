import Joi from 'joi';

export const CREATE = Joi.object({
  text: Joi.string().required(),
  recipient: Joi.number().integer().required(),
  created_at: Joi.date(),
});

export const UPDATE = Joi.object({
  text: Joi.string().required(),
});

export const LIST = {
  recipient: Joi.number().integer().required(),
};

export default {
  CREATE,
  UPDATE,
  LIST,
};
