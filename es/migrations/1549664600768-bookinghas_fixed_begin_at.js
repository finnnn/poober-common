const db = require('../utils/db');
const TABLES = require('../constants/tables');

module.exports.up = next => {
  db.schema.table(TABLES.BOOKINGS, table => {
    if (!table) {
      next();
      return;
    }
    table.boolean('has_fixed_begin_at');
  })
  .then(() => {
    setTimeout(next);
  });
};

module.exports.down = next => {
  console.log('down');
  next();
};
