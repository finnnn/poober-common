const db = require('../utils/db');
const { createTable, dropTable } = require('../utils/migrate')(db);
const TABLES = require('../constants/tables');

module.exports.up = next => {
  createTable(TABLES.SETTINGS_NOTIFICATIONS, table => {
    if (table) {
      table.increments();
      table.integer('user');
      table.string('notification_type');
      table.timestamps(); 
    }
    setTimeout(next);
  });
};

module.exports.down = next => {
  dropTable(TABLES.NOTIFICATIONS, next);
};
