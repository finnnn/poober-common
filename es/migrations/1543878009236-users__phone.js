const db = require('../utils/db');
const TABLES = require('../constants/tables');

module.exports.up = next => {
  db.schema.table(TABLES.USERS, (table) => {
    if (!table) {
      next();
      return;
    }
    table.string('phone_number');
    table.string('phone_country');
    table.string('phone_validation_code');
    table.boolean('phone_validated');
    table.date('phone_validation_code_created_at');
    table.string('email_validation_code');
    table.boolean('email_validated');
    table.date('email_validation_code_created_at');
  }).then(() => {
    setTimeout(next);
  });
};

module.exports.down = next => {
  console.log('down');
  next();
};
