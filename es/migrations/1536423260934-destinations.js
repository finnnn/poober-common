const db = require('../utils/db');
const TABLES = require('../constants/tables');

const { createTable, dropTable } = require('../utils/migrate')(db);

module.exports.up = next => {
  createTable(TABLES.DESTINATIONS, table => {
    if (!table) {
      next();
      return;
    }
    table.increments();
    table.string('name');
    table.timestamps();
    setTimeout(next, 1000);
  });
};

module.exports.down = next => {
  console.log('down');
  dropTable(TABLES.DESTINATIONS, next);
};
