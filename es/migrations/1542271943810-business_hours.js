const db = require('../utils/db');
const TABLES = require('../constants/tables');

const { createTable, dropTable } = require('../utils/migrate')(db);

module.exports.up = next => {
  createTable(TABLES.BUSINESS_HOURS, table => {
    if (!table) {
      next();
      return;
    }
    table.increments();
    table.enu('entity_type', ['destination'])
    table.integer('entity_id');
    table.string('timezone');
    table.integer('day');
    table.integer('opening_time');
    table.integer('closing_time');
    table.timestamps();
    setTimeout(next, 1000);
  });
};

module.exports.down = next => {
  console.log('down');
  dropTable(TABLES.BUSINESS_HOURS, next);
};
