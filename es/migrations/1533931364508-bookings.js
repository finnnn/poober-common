const db = require('../utils/db');
const { createTable, dropTable } = require('../utils/migrate')(db);
const TABLES = require('../constants/tables');

module.exports.up = next => {
  createTable(TABLES.BOOKINGS, table => {
    if (!table) {
      next();
      return;
    }
    table.increments();
    table.timestamps();
    table.integer('requester');
    table.integer('destination');
    table.timestamp('begin_at');
    table.timestamp('end_at');
    table.integer('begin_day');
    setTimeout(next);
  });
};

module.exports.down = next => {
  dropTable(TABLES.BOOKINGS, next);
};
