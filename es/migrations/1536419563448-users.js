const db = require('../utils/db');
const { createTable, dropTable } = require('../utils/migrate')(db);
const TABLES = require('../constants/tables');

module.exports.up = next => {
  createTable(TABLES.USERS, table => {
    if (table) {
      table.increments();
      table.string('name');
      table.string('email');
      table.string('password');
      table.timestamps(); 
    }
    setTimeout(next);
  });
};

module.exports.down = next => {
  dropTable(TABLES.USERS, next);
};
