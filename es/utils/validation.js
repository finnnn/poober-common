import Joi from 'joi';

export const query = (schema) => (req, res, next) => {
  Joi.validate(req.query, schema, { allowUnknown: true }, next);
};

export const body = (schema) => (req, res, next) => {
  Joi.validate(req.body, schema, {}, next);
};

export const headers = (schema) => (req, res, next) => {
  Joi.validate(req.headers, schema, { allowUnknown: true }, next);
};

export const socket = (schema, payload, next) => {
  Joi.validate(payload, schema, {}, next);
};
