const tap = require('tap');
const moment = require('moment');

tap.Test.prototype.addAssert('equalTime', 2, function(_actual, _expected, message, extra) {
  const expected = _expected.getTime();
  const actual = _actual.getTime();

  return this.equal(
    expected,
    actual,
    message || 'expected ' + _expected + ' to equal ' + _actual,
    extra
  );
});

tap.Test.prototype.addAssert('equalDate', 2, function(_actual, _expected, message, extra) {
  const expected = _expected.toDateString();
  const actual = _actual.toDateString();

  return this.equal(
    expected,
    actual,
    message || 'expected ' + _expected + ' to equal ' + _actual,
    extra
  );
});

export const roundDate = date => {
  const momentDate = moment(date);
  const seconds = momentDate.seconds();
  if (Math.round(seconds / 60)) {
    momentDate.add(1, 'minutes');
  }
  return momentDate
    .seconds(0)
    .toString();
};