// made this because boom wasnt working on react native
const STATUS_CODES = {
  notFound: 404,
  tooManyRequests: 429,
};

const STATUS_TEXT = {
  notFound: 'Not Found',
  tooManyRequests: 'Too Many Requests',
};

export const notFound = (message) => {
  const result = new Error(message);
  result.isBoom = true;
  result.output = {
    statusCode: STATUS_CODES.notFound,
    payload: {
      statusCode: STATUS_CODES.notFound,
      error: STATUS_TEXT.notFound,
      message,
    },
  };
  return result;
};

export const tooManyRequests = (message) => {
  const result = new Error(message);
  result.isBoom = true;
  result.output = {
    statusCode: STATUS_CODES.tooManyRequests,
    payload: {
      statusCode: STATUS_CODES.tooManyRequests,
      error: STATUS_TEXT.tooManyRequests,
      message,
    },
  };
  return result;
};

export default {
  notFound,
  tooManyRequests,
};
