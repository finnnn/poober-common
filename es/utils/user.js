import boom from './boom';
import usersServiceFactory from '../services/users';
import db from './db';

const usersService = usersServiceFactory(db);

const VALIDATION_PERIOD_MINUTES = 10;

export const validatePhoneNumber = async (id, validationCode) => {
  const user = await usersService.get(id);

  if (!user) {
    return false;
  }

  if (user.phone_validated) {
    return true;
  }

  if (moment(user.phone_validation_code_created_at).add(VALIDATION_PERIOD_MINUTES, 'minutes').isAfter(moment())) {
    throw boom.tooManyRequests('validation period expired');
  }

  if (user.phone_validation_code === validationCode) {
    await usersService.update(id, { phone_validated: true });
    return true;
  }

  return false;
};

export const validateEmail = async (id, validationCode) => {
  const user = await usersService.get(id);

  if (!user) {
    return false;
  }

  if (user.email_validated) {
    return true;
  }

  if (moment(user.email_validation_code_created_at).add(VALIDATION_PERIOD_MINUTES, 'minutes').isAfter(moment())) {
    throw boom.tooManyRequests('validation period expired');
  }

  if (user.email_validation_code === validationCode) {
    await usersService.update(id, { email_validated: true });
    return true;
  }

  return false;
};

export default {
  validatePhoneNumber,
  validateEmail,
};
