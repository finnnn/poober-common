// require('../test');
// const { test } = require('tap');
// const moment = require('moment');

// const db = require('../db');
// const { isAvailable } = require('../destination');
// const TABLES = require('../../constants/tables');
// const destinationsService = require('../../services/destinations')(db);
// const bookingsService = require('../../services/bookings')(db);

// const randomPoint = (x = 0, y = 0) => ({ lat: x, lon: y });
// const testData = {};
// const currentTime = moment()
//   .hour(0)
//   .minute(0)
//   .second(0)
//   .millisecond(0)
//   .toDate();
// const currentDay = moment(currentTime).day();
// const currentMinute = moment(currentTime).minutes();
// const TWENTY_FOUR = 24 * 60;
// const TWENTY_FOUR_SEVEN = {
//   0: { opening_time: 0, closing_time: TWENTY_FOUR },
//   1: { opening_time: 0, closing_time: TWENTY_FOUR },
//   2: { opening_time: 0, closing_time: TWENTY_FOUR },
//   3: { opening_time: 0, closing_time: TWENTY_FOUR },
//   4: { opening_time: 0, closing_time: TWENTY_FOUR },
//   5: { opening_time: 0, closing_time: TWENTY_FOUR },
//   6: { opening_time: 0, closing_time: TWENTY_FOUR },
// };

// test('utils/destination', (t) => {
//   t.test('intialize db', async (t) => {
//     db.initialize();
//     t.end();
//   });

//   t.test('truncate tables', async (t) => {
//     await db(TABLES.DESTINATIONS).truncate();
//     await db(TABLES.BOOKINGS).truncate();
//     await db(TABLES.BUSINESS_HOURS).truncate();
//     t.end();
//   });

//   t.test('seed data', async t => {
//     const destA = await destinationsService.create({
//       name: `DEST A`,
//       location: randomPoint(2, 0),
//       business_hours: TWENTY_FOUR_SEVEN,
//     });

//     const destB = await destinationsService.create({
//       name: `DEST B`,
//       location: randomPoint(2, 0),
//       business_hours: TWENTY_FOUR_SEVEN,
//     });

//     const destC = await destinationsService.create({
//       name: `DEST C`,
//       location: randomPoint(3, 0),
//       business_hours: TWENTY_FOUR_SEVEN,
//     });

//     const destD = await destinationsService.create({
//       name: `DEST D`,
//       location: randomPoint(5, 0),
//       business_hours: TWENTY_FOUR_SEVEN,
//     });

//     const destE = await destinationsService.create({
//       name: `DEST E`,
//       location: randomPoint(6, 0),
//       business_hours: TWENTY_FOUR_SEVEN,
//     });

//     // business_hours before
//     const destG = await destinationsService.create({
//       name: `DEST G`,
//       location: randomPoint(2, 0),
//       business_hours: {
//         ...TWENTY_FOUR_SEVEN,
//         [currentDay]: {
//           opening_time: currentMinute - 60,
//           closing_time: currentMinute - 30,
//         },
//       },
//     });

//     // business_hours after
//     const destH = await destinationsService.create({
//       name: `DEST H`,
//       location: randomPoint(2, 0),
//       business_hours: {
//         ...TWENTY_FOUR_SEVEN,
//         [currentDay]: {
//           opening_time: currentMinute + 60,
//           closing_time: currentMinute + 90,
//         },
//       },
//     });

//     // business_hours overlapping left
//     const destI = await destinationsService.create({
//       name: `DEST I`,
//       location: randomPoint(2, 0),
//       business_hours: {
//         ...TWENTY_FOUR_SEVEN,
//         [currentDay]: {
//           opening_time: currentMinute - 5,
//           closing_time: currentMinute + 10,
//         },
//       },
//     });

//     // business_hours overlapping right
//     const destJ = await destinationsService.create({
//       name: `DEST J`,
//       location: randomPoint(2, 0),
//       business_hours: {
//         ...TWENTY_FOUR_SEVEN,
//         [currentDay]: {
//           opening_time: currentMinute + 5,
//           closing_time: currentMinute + 20,
//         },
//       },
//     });

//     // business_hours inside
//     const destK = await destinationsService.create({
//       name: `DEST K`,
//       location: randomPoint(2, 0),
//       business_hours: {
//         ...TWENTY_FOUR_SEVEN,
//         [currentDay]: {
//           opening_time: currentMinute + 5,
//           closing_time: currentMinute + 10,
//         },
//       },
//     });

//     // DestB bookings - either side
//     const destBBookings = await Promise.all([
//       bookingsService.create({
//         destination: destB.id,
//         begin_at: moment(currentTime).subtract(20, 'minutes').toDate(),
//         end_at: moment(currentTime).subtract(5, 'minutes').toDate(),
//       }),
//       bookingsService.create({
//         destination: destB.id,
//         begin_at: moment(currentTime).add(20, 'minutes').toDate(),
//         end_at: moment(currentTime).add(35, 'minutes').toDate(),
//       }),
//     ]);

//     // DestC bookings - overlapping left
//     const destCBookings = await bookingsService.create({
//       destination: destC.id,
//       begin_at: moment(currentTime).subtract(10, 'minutes').toDate(),
//       end_at: moment(currentTime).add(5, 'minutes').toDate(),
//     });

//     // DestD bookings - overlapping right
//     const destDBookings = await bookingsService.create({
//       destination: destD.id,
//       begin_at: moment(currentTime).add(10, 'minutes').toDate(),
//       end_at: moment(currentTime).add(25, 'minutes').toDate(),
//     });

//     // DestE bookings - contained
//     const destEBookings = await bookingsService.create({
//       destination: destE.id,
//       begin_at: moment(currentTime).add(5, 'minutes').toDate(),
//       end_at: moment(currentTime).add(10, 'minutes').toDate(),
//     });

//     testData.destA = destA;
//     testData.destB = destB;
//     testData.destC = destC;
//     testData.destD = destD;
//     testData.destE = destE;
//     testData.destG = destG;
//     testData.destH = destH;
//     testData.destI = destI;
//     testData.destJ = destJ;
//     testData.destK = destK;
//     testData.destBBookings = destBBookings;
//     testData.destCBookings = destCBookings;
//     testData.destDBookings = destDBookings;
//     testData.destEBookings = destEBookings;

//     t.end();
//   });

//   // t.test('isAvailable()', async (t) => {
//   //   t.plan(10);

//   //   const now = moment(currentTime);

//   //   const resultA = await isAvailable({
//   //     destination: testData.destA.id,
//   //     beginAt: now.clone().toDate(),
//   //     endAt: now.clone().add(15, 'minutes').toDate(),
//   //     // timezone,
//   //   });
//   //   t.ok(resultA, 'resultA');

//   //   const resultB = await isAvailable({
//   //     destination: testData.destB.id,
//   //     beginAt: now.clone().toDate(),
//   //     endAt: now.clone().add(15, 'minutes').toDate(),
//   //     // timezone,
//   //   });
//   //   t.ok(resultB, 'resultB');
//   //   const resultC = await isAvailable({
//   //     destination: testData.destC.id,
//   //     beginAt: now.clone().toDate(),
//   //     endAt: now.clone().add(15, 'minutes').toDate(),
//   //     // timezone,
//   //   });
//   //   t.throws(resultC, 'resultC');

//   //   const resultD = await isAvailable({
//   //     destination: testData.destD.id,
//   //     beginAt: now.clone().toDate(),
//   //     endAt: now.clone().add(15, 'minutes').toDate(),
//   //     // timezone,
//   //   });
//   //   t.throws(resultD, 'resultD');

//   //   const resultE = await isAvailable({
//   //     destination: testData.destE.id,
//   //     beginAt: now.clone().toDate(),
//   //     endAt: now.clone().add(15, 'minutes').toDate(),
//   //     // timezone,
//   //   });
//   //   t.throws(resultE, 'resultE');

//   //   // business_hours before
//   //   const resultG = await isAvailable({
//   //     destination: testData.destG.id,
//   //     beginAt: now.clone().toDate(),
//   //     endAt: now.clone().add(15, 'minutes').toDate(),
//   //     // timezone,
//   //   });
//   //   t.throws(resultG, 'resultG');

//   //   // business_hours after
//   //   const resultH = await isAvailable({
//   //     destination: testData.destH.id,
//   //     beginAt: now.clone().toDate(),
//   //     endAt: now.clone().add(15, 'minutes').toDate(),
//   //     // timezone,
//   //   });
//   //   t.throws(resultH, 'resultH');

//   //   // business_hours overlapping left
//   //   const resultI = await isAvailable({
//   //     destination: testData.destI.id,
//   //     beginAt: now.clone().toDate(),
//   //     endAt: now.clone().add(15, 'minutes').toDate(),
//   //     // timezone,
//   //   });
//   //   t.throws(resultI, 'resultI');

//   //   // business_hours overlapping right
//   //   const resultJ = await isAvailable({
//   //     destination: testData.destJ.id,
//   //     beginAt: now.clone().toDate(),
//   //     endAt: now.clone().add(15, 'minutes').toDate(),
//   //     // timezone,
//   //   });
//   //   t.throws(resultJ, 'resultJ');

//   //   // business_hours inside
//   //   const resultK = await isAvailable({
//   //     destination: testData.destK.id,
//   //     beginAt: now.clone().toDate(),
//   //     endAt: now.clone().add(15, 'minutes').toDate(),
//   //     // timezone,
//   //   });
//   //   t.throws(resultK, 'resultK');
//   // });

//   t.test('close DB', (tt) => {
//     db.destroy(() => {
//       tt.end();
//     });
//   });
//   console.log('blah');
//   t.end();
// });
