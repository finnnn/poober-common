require('../test');
const tap = require('tap');
const Joi = require('joi');

const { query, body, headers } = require('../validation');

const schema = Joi.object({
  a: Joi.string().required(),
  b: Joi.number(),
  c: Joi.boolean(),
});

tap.test('utils/validation', (t) => {
  t.plan(8);

  t.doesNotThrow(async () => {
    await new Promise((resolve) => {
      query(schema)({
        query: {
          a: 'foo',
          b: 1,
          c: true,
          d: 'bar'
        },
      }, null, (error) => {
        t.notOk(error);
        resolve();
      });
    });

    await new Promise((resolve) => {
      query(schema)({
        query: {
          b: 1,
          c: true,
          d: 'bar',
        },
      }, null, (error) => {
        t.ok(error);
        resolve();
      });
    });

    await new Promise((resolve) => {
      body(schema)({
        body: {
          a: 'foo',
          b: 1,
          c: true,
        },
      }, null, (error) => {
        t.notOk(error);
        resolve();
      });
    });

    await new Promise((resolve) => {
      body(schema)({
        body: {
          b: 1,
          c: true,
        },
      }, null, (error) => {
        t.ok(error);
        resolve();
      });
    });

    await new Promise((resolve) => {
      body(schema)({
        body: {
          a: 'foo',
          b: 1,
          c: true,
          d: 'bar',
        },
      }, null, (error) => {
        t.ok(error);
        resolve();
      });
    });

    await new Promise((resolve) => {
      headers(schema)({
        headers: {
          a: 'foo',
          b: 1,
          c: true,
          d: 'bar',
        },
      }, null, (error) => {
        t.notOk(error);
        resolve();
      });
    });

    await new Promise((resolve) => {
      headers(schema)({
        headers: {
          b: 1,
          c: true,
          d: 'bar',
        },
      }, null, (error) => {
        t.ok(error);
        resolve();
      });
    });
  });

});
 