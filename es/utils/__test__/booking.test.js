require('../test');
const tap = require('tap');
const moment = require('moment');

const { syncTimeWindow, canTransitionToStatus } = require('../booking');
import {
  APPROVAL_PENDING,
  APPROVAL_DECLINED,
  APPROVAL_EXPIRED,
  COMMENCEMENT_PENDING,
  COMMENCEMENT_SUCCESSFUL,
  COMMENCEMENT_PROBLEMATIC,
  EXECUTION_SUCCESSFUL,
  CANCELLED,
} from '../../constants/bookingStatuses';

tap.test('utils/booking', (t) => {
  t.test('syncTimeWindow()', (t) => {
    t.plan(2);

    const now = moment();

    const result = syncTimeWindow({
      begin_at: now.clone().subtract(10, 'minutes').toDate(),
      end_at: now.clone().add(20, 'minutes').toDate(),
    }, now.toDate());

    t.equalDate(result.begin_at, now.clone().add(10, 'minutes').toDate());
    t.equalDate(result.end_at, now.clone().add(30, 'minutes').toDate());
  });

  t.test('canTransitionToStatus()', t => {
    t.plan(16);
    const idPopeye = 1;
    const idOlive = 2;
    
    t.ok(canTransitionToStatus({
      booking: {
        requester: { id: idPopeye },
        destination: { owner: { id: idOlive }, },
        status: APPROVAL_PENDING,
      },
      status: COMMENCEMENT_PENDING,
      userId: idOlive,
    }));
    
    t.ok(canTransitionToStatus({
      booking: {
        requester: { id: idPopeye },
        destination: { owner: { id: idOlive }, },
        status: APPROVAL_PENDING,
      },
      status: CANCELLED,
      userId: idPopeye,
    }));
    
    t.notOk(canTransitionToStatus({
      booking: {
        requester: { id: idPopeye },
        destination: { owner: { id: idOlive }, },
        status: APPROVAL_PENDING,
      },
      status: CANCELLED,
      userId: idOlive,
    }));
    
    t.notOk(canTransitionToStatus({
      booking: {
        requester: { id: idPopeye },
        destination: { owner: { id: idOlive }, },
        status: COMMENCEMENT_SUCCESSFUL,
      },
      status: CANCELLED,
      userId: idPopeye,
    }));
    
    t.ok(canTransitionToStatus({
      booking: {
        requester: { id: idPopeye },
        destination: { owner: { id: idOlive }, },
        status: APPROVAL_PENDING,
      },
      status: APPROVAL_DECLINED,
      userId: idOlive,
    }));
    
    t.ok(canTransitionToStatus({
      booking: {
        requester: { id: idPopeye },
        destination: { owner: { id: idOlive }, },
        status: COMMENCEMENT_PENDING,
      },
      status: COMMENCEMENT_SUCCESSFUL,
      userId: idPopeye,
    }));
    
    t.ok(canTransitionToStatus({
      booking: {
        requester: { id: idPopeye },
        destination: { owner: { id: idOlive }, },
        status: COMMENCEMENT_PENDING,
      },
      status: COMMENCEMENT_PROBLEMATIC,
      userId: idPopeye,
    }));
    
    t.ok(canTransitionToStatus({
      booking: {
        requester: { id: idPopeye },
        destination: { owner: { id: idOlive }, },
        status: COMMENCEMENT_SUCCESSFUL,
      },
      status: EXECUTION_SUCCESSFUL,
      userId: idPopeye,
    }));
    
    t.notOk(canTransitionToStatus({
      booking: {
        requester: { id: idPopeye },
        destination: { owner: { id: idOlive }, },
        status: APPROVAL_PENDING,
      },
      status: EXECUTION_SUCCESSFUL,
      userId: idOlive,
    }));
    
    t.notOk(canTransitionToStatus({
      booking: {
        requester: { id: idPopeye },
        destination: { owner: { id: idOlive }, },
        status: APPROVAL_PENDING,
      },
      status: COMMENCEMENT_PENDING,
      userId: idPopeye,
    }));
    
    t.notOk(canTransitionToStatus({
      booking: {
        requester: { id: idPopeye },
        destination: { owner: { id: idOlive }, },
        status: APPROVAL_DECLINED,
      },
      status: COMMENCEMENT_PENDING,
      userId: idPopeye,
    }));
    
    t.notOk(canTransitionToStatus({
      booking: {
        requester: { id: idPopeye },
        destination: { owner: { id: idOlive }, },
        status: APPROVAL_EXPIRED,
      },
      status: COMMENCEMENT_PENDING,
      userId: idPopeye,
    }));
    
    t.notOk(canTransitionToStatus({
      booking: {
        requester: { id: idPopeye },
        destination: { owner: { id: idOlive }, },
        status: COMMENCEMENT_PENDING,
      },
      status: EXECUTION_SUCCESSFUL,
      userId: idPopeye,
    }));
    
    t.notOk(canTransitionToStatus({
      booking: {
        requester: { id: idPopeye },
        destination: { owner: { id: idOlive }, },
        status: COMMENCEMENT_PENDING,
      },
      status: COMMENCEMENT_SUCCESSFUL,
      userId: idOlive,
    }));
    
    t.notOk(canTransitionToStatus({
      booking: {
        requester: { id: idPopeye },
        destination: { owner: { id: idOlive }, },
        status: COMMENCEMENT_SUCCESSFUL,
      },
      status: EXECUTION_SUCCESSFUL,
      userId: idOlive,
    }));
    
    t.notOk(canTransitionToStatus({
      booking: {
        requester: { id: idPopeye },
        destination: { owner: { id: idOlive }, },
        status: COMMENCEMENT_SUCCESSFUL,
      },
      status: COMMENCEMENT_PROBLEMATIC,
      userId: idPopeye,
    }));
  });
  
  t.end();
});
