require('../test');
const tap = require('tap');
const { init, subscribe, publish, close } = require('../mq');

tap.test('utils/mq', (t) => {
  t.test('init()', async (t) => {
    await init();
    t.end();
  });
  
  t.test('subscribe() + publish()', (t) => {
    t.plan(2);
    subscribe(({ message: { type, payload } }) => {
      t.equal(type, 'TEST');
      t.match(payload, { foo: 'bar' });
      t.end();
    });
    publish({ type: 'TEST', payload: { foo: 'bar' } });
  });
  
  t.test('close()', async (t) => {
    await close();
    t.end();
  });

  t.end();
});
