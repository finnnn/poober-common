import { isNaN } from 'lodash';

export const int = value => {
  const result = parseInt(value);
  return isNaN(result) ? null : result;
};
