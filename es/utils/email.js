import SendGrid from '@sendgrid/mail';

export const init = (apiKey) => {
  SendGrid.setApiKey(apiKey);
};

export const send = ({ to, from, subject, template, templateData }) => {
  SendGrid.send({
    to,
    from,
    subject,
    html: require(`../emailTemplates/html/${template}`)(templateData),
    text: require(`../emailTemplates/text/${template}`)(templateData),
  });
};

// // using SendGrid's v3 Node.js Library
// // https://github.com/sendgrid/sendgrid-nodejs
// const msg = {
//   to: 'test@example.com',
//   from: 'test@example.com',
//   subject: 'Sending with SendGrid is Fun',
//   text: 'and easy to do anywhere, even with Node.js',
//   html: '<strong>and easy to do anywhere, even with Node.js</strong>',
// };