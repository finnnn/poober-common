const ascoltatori = require('ascoltatori');
const EVENT_PRIORITIES = require('../constants/eventPriorities');

let bus;
// const handlers = [];

export const init = () => new Promise((resolve, reject) => {
  const settings = {
    type: 'mongo',
    url: process.env.POOBER_MONGO_URL,
    pubsubCollection: 'mq',
  };
  
  ascoltatori.build(settings, (err, ascoltatore) => {
    if (err) {
      reject(err);
      return;
    }
    bus = ascoltatore;
    resolve(bus);
  });
});

export const subscribe = (handler) => {
  bus.subscribe(EVENT_PRIORITIES.DEFAULT, (priority, message) => {
    handler({ priority, message: JSON.parse(message) });
  });
};

export const publish = ({ type, payload }) => {
  bus.publish(EVENT_PRIORITIES.DEFAULT, JSON.stringify({ type, payload }), () => {});
};

// export const unsubscribe = async (topic) =>
//   Promise.all((handlers[topic] || []).map(handler => new Promise(resolve => {
//     bus.unsubscribe(topic, handler, resolve);
//   })));

export const close = async () => new Promise((resolve) => {
  bus.close(() => {
    console.log('closed mq');
    resolve();
  });
});

