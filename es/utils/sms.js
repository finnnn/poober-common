import Twilio from 'twilio';

let client;
let from;

export const init = ({ sid, token, phoneNumber }) => {
  client = Twilio(sid, token);
  from = phoneNumber;
  return client;
};

export const send = ({ body, to }) =>
  new Promise(resolve => {
    if (!client) {
      resolve();
      return;
    }

    client.messages
      .create({
        body,
        to,
        from,
      })
      .then(resolve)
      .done();
  });
