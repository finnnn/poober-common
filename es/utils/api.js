export default ({
  baseUrl,
  fetch,
  getJwtToken = () => {
    throw new Error('getJwtToken not configured');
  },
  onError = () => {
    throw new Error('onError not configured');
  },
  onAuthError = () => {
    throw new Error('onAuthError not configured');
  },
}) => {

  const handleResponse = async response => {
    const {
      headers,
      ok,
      redirected,
      status,
      statusText,
      type,
      url,
      useFinalURL,
      bodyUsed,
    } = response;

    if (status === 401) {
      onAuthError(response);
    }
    let body;

    if (ok) {
      try {
        body = await response.json();
      } catch(error) {
        onError(error);
      }
    }

    return {
      body,
      headers,
      ok,
      redirected,
      status,
      statusText,
      type,
      url,
      useFinalURL,
      bodyUsed,
    };
  };
   
  let response;
  return {
    get: async (uri, ...params) => {
      try {
        const jwtToken = await getJwtToken();
        response = await fetch(`${baseUrl}${uri}`, {
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: jwtToken,
          },
          ...params,
        });
      } catch (error) {
        return onError(error);
      }
      return await handleResponse(response);
    },
    
    post: async (uri, { body, ...params }) => {
      try {
        const jwtToken = await getJwtToken();
        const response = await fetch(`${baseUrl}${uri}`, {
          method: 'post',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            Authorization: jwtToken,
          },
          body: JSON.stringify(body),
          ...params,
        })
        return await handleResponse(response);
      } catch (error) {
        return onError(error);
      }
    },
    
    put: async (uri, { body, ...params }) => {
      try {
        const jwtToken = await getJwtToken();
        const response = await fetch(`${baseUrl}${uri}`, {
          method: 'put',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            Authorization: jwtToken,
          },
          body: JSON.stringify(body),
          ...params,
        })
        return await handleResponse(response);
      } catch (error) {
        return onError(error);
      }
    },
    
    delete: async (uri, { ...params }) => {
      try {
        const jwtToken = await getJwtToken();
        const response = await fetch(`${baseUrl}${uri}`, {
          method: 'delete',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            Authorization: jwtToken,
          },
          ...params,
        })
        return await handleResponse(response);
      } catch (error) {
        return onError(error);
      }
    },
  };
};
