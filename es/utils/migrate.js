const path = require('path');
const migrate = require('migrate');

const TABLES = require('../constants/tables');

module.exports = db => {
  const stateStore = {
    load: async (fn) => {
      const row = await db(TABLES.MIGRATIONS).first();
      if (row) {
        console.log(JSON.parse(row.data));
      }
      return fn(null, row ? JSON.parse(row.data) : {});
    },
  
    save: async (set, fn) => {
      const migrationData = {
        lastRun: set.lastRun,
        migrations: set.migrations.map(migration => ({
          ...migration,
          timestamp: migration.timestamp || (new Date()).getTime(),
        })),
      };
      await db(TABLES.MIGRATIONS).truncate();
      await db(TABLES.MIGRATIONS).insert({ data: JSON.stringify(migrationData) });
      fn();
    },
  };
  
  const createTable = (tableName, callback) => {
    return db.schema.hasTable(tableName).then(exists => {
      if (exists) {
        callback();
        return;
      }
      return db.schema.createTable(tableName, callback);
    })
    .catch(err => {
      console.error(err);
    });
  };

  return {
    up: () => new Promise(async (resolve, reject) => {
      try {
        await createTable(TABLES.MIGRATIONS, table => {
          if (table) {
            table.text('data');
          }
        });
      }
      catch(error) {
        return;
      }

      migrate.load({
        stateStore,
        migrationsDirectory: path.join(__dirname, '../migrations'),
      },  (error, set) => {
        if (error) {
          reject(error);
          return;
        }
        set.up((error) => {
          if (error) { 
            reject(error);
            return;
          }
          resolve();
        }); 
      });
    }),
    createTable,
    dropTable: (tableName, callback) => {
      console.log('dropTable', tableName);
      db.schema.hasTable(tableName).then(exists => {
        if (!exists) {
          callback();
          return;
        }
        db.schema.dropTable(tableName).then(() => {
          callback();
        });
      });
    },
  };
};
