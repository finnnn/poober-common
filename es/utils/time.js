import moment from 'moment';
import itsSet from 'its-set';

export const dateToMinsFromMidnight = date => { 
  const d = moment(date);
  return d.get('minute') + d.get('hour') * 60;
};

export const dayOfWeek = date => moment(date).format('d');

export const getTimezone = date => moment(date).format('ZZ');

export const humanReadableTime = minsFromMidnight => {
  if (!itsSet(minsFromMidnight)) return null;
  const hours = Math.floor(minsFromMidnight / 60);
  const hourMinutes = hours * 60;
  const minutes = minsFromMidnight - hourMinutes;
  return moment()
    .set('hour', hours)
    .set('minute', minutes)
    .format('HH:mm');
};
