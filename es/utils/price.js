const MINUTES_PER_INTERVAL = 5;
const PRICE_PER_INTERVAL = 1;
const URGENT_PRICE = 1;
const URGENT_CUT_OFF_MINUTES = 10;
const QUEUE_SIZE_CUT_OFF = 0;
const SHORT_QUEUE_PRICE = 1;
const AUTO_ACCEPT_PRICE = 1;

export const calculateBookingPrice = ({
  duration,
  queue_size_limit,
  auto_approve_enabled,
  urgency,
}) => {
  let price = (duration / MINUTES_PER_INTERVAL) * PRICE_PER_INTERVAL;

  price += urgency < URGENT_CUT_OFF_MINUTES ? URGENT_PRICE : 0;

  price += queue_size_limit === QUEUE_SIZE_CUT_OFF ? SHORT_QUEUE_PRICE : 0;

  price += auto_approve_enabled ? AUTO_ACCEPT_PRICE : 0

  return price;
};
