const knex = require('knex');
const knexPostgis = require('knex-postgis');

const db = knex({
  client: 'pg',
  dialect: 'postgres',
  searchPath: ['knex', 'public', 'postgis'],
  connection: process.env.POOBER_DB_URL,
});

knexPostgis(db);

export default db;