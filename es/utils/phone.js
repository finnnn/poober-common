import phone from 'phone';
import countrycodes from 'countrycodes/countryCodes';

export const validatePhoneNumber = (phoneNumber, countryCode) => {
  if (!phoneNumber || !countryCode) return false;

  const [normalisedPhoneNumber] = phone(
    phoneNumber.replace('+', ''),
    countrycodes.getISO3(countryCode)
  );
  
  if (!normalisedPhoneNumber) return false;
  
  return true;
};

export const normalizePhoneNumber = (number) => {
  const [phoneNumber, countryCode] = phone(number);
  return phoneNumber ? { phoneNumber, countryCode } : null;
};
