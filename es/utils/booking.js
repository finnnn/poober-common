import moment from 'moment';
import {
  APPROVAL_PENDING,
  APPROVAL_DECLINED,
  APPROVAL_EXPIRED,
  COMMENCEMENT_PENDING,
  COMMENCEMENT_SUCCESSFUL,
  COMMENCEMENT_PROBLEMATIC,
  EXECUTION_SUCCESSFUL,
  CLOSED,
  CANCELLED,
} from '../constants/bookingStatuses';
import { DEFAULT_BOOKING_COMMENCEMENT_PERIOD_MINUTES, DEFAULT_BOOKING_PERIOD_MINUTES } from '../constants/timing';
const bookingsServiceFactory = require('../services/bookings');
 
export const canTransitionToStatus = ({ booking, status, userId }) => {
  const outbound = booking.requester.id === userId;
  const inbound = booking.destination.owner.id === userId;
  switch (booking.status) {
    case APPROVAL_PENDING: {
      if (outbound) {
        if ([
          CANCELLED,
        ].includes(status)) {
          return true;
        }
        return false;
      }
      if ([
        COMMENCEMENT_PENDING,
        APPROVAL_DECLINED,
      ].includes(status)) {
        return true;
      }
      break;
    }
    case APPROVAL_DECLINED:
    case APPROVAL_EXPIRED: {
      return false;
    }
    case COMMENCEMENT_PENDING: {
      if (inbound) return false;
      if ([
        COMMENCEMENT_SUCCESSFUL,
        COMMENCEMENT_PROBLEMATIC,
        CANCELLED,
      ].includes(status)) {
        return true;
      }
      break;
    }
    case COMMENCEMENT_SUCCESSFUL: {
      if (inbound) return false;
      if ([
        EXECUTION_SUCCESSFUL,
      ].includes(status)) {
        return true;
      }
      break;
    }
    case EXECUTION_SUCCESSFUL: {
      if (inbound) return false;
      if ([
        CLOSED,
      ].includes(status)) {
        return true;
      }
      break;
    }
    // no default
  }
  return false;
};

export const syncTimeWindow = (booking, to, opts) => {
  const { offset } =  { offset: 10, ...opts };
  const beginAt = moment(to).add(offset, 'minutes');
  const period = moment(booking.end_at).diff(booking.begin_at, 'minutes');
  const end_at = beginAt.clone().add(period, 'minutes').toDate();
  return {
    ...booking,
    begin_at: beginAt.toDate(),
    end_at,
  };
};

export const syncPeerBookings = async (booking, db) => {
  const bookingsService = bookingsServiceFactory(db);
  const peerBookings = await bookingsService.find({
    destination: booking.destination.id,
    active: true,
  });

  if (!peerBookings.length) {
    return [];
  }

  const sortedPeerBookings = peerBookings.sort((a, b) => new Date(a.begin_at) - new Date(b.begin_at));

  return Promise.all(sortedPeerBookings.map(async (peerBooking, i) => {
    const now = moment(new Date());
    const autoBeginAt = moment(peerBooking.created_at).add(
      DEFAULT_BOOKING_COMMENCEMENT_PERIOD_MINUTES + i * DEFAULT_BOOKING_PERIOD_MINUTES,
      'minutes'
    );
    const earliestBeginAt = moment(peerBooking.created_at)
      .add(DEFAULT_BOOKING_COMMENCEMENT_PERIOD_MINUTES);
    const beginAt = moment.max(now, moment.max(autoBeginAt, earliestBeginAt)).toDate();
    const updatedPeerBooking = await bookingsService.update(peerBooking.id, {
      begin_at: beginAt,
    });

    return { peerBooking, updatedPeerBooking };
  }));
}

export default {
  canTransitionToStatus,
  syncTimeWindow,
  syncPeerBookings,
};
