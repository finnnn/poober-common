const POINT_MAPPING = ['latitude', 'longitude'];

export const pointToJson = point => point
  .replace('POINT(', '')
  .replace(')', '')
  .split(' ')
  .reduce((acc, val, i) => ({
    ...acc,
    [POINT_MAPPING[i]]: parseFloat(val),
  }), {});

export const jsonToPoint = (args) => {
  const { db, point: { lat, lon } } = args;
  return db.postgis.geomFromText(`Point(${lat} ${lon})`, 4326);
};

export default {
  pointToJson,
  jsonToPoint,
};
