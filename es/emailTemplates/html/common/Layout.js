import React from 'react';
import {
  render,
  Mjml,
  MjmlHead,
  MjmlTitle,
  MjmlPreview,
  MjmlBody,
} from 'mjml-react';

export default ({ children, title, preview }) => {
  const { html, errors } = render((
    <Mjml>
      <MjmlHead>
        <MjmlTitle>{title}</MjmlTitle>
        <MjmlPreview>{preview}</MjmlPreview>
      </MjmlHead>
      <MjmlBody width={500}>
        {/* <MjmlSection fullWidth backgroundColor="#efefef">
          <MjmlColumn>
            <MjmlImage src="https://static.wixstatic.com/media/5cb24728abef45dabebe7edc1d97ddd2.jpg"/>
          </MjmlColumn>
        </MjmlSection> */}
        {children}
      </MjmlBody>
    </Mjml>
  ), {validationLevel: 'soft'});

  return html;
};
