import React from 'react';
import {
  MjmlButton,
  MjmlSection,
  MjmlColumn,
} from 'mjml-react';

import Layout from './common/Layout';

export default ({ confirmEmailUrl }) => (
  <Layout
    title="Confirm Email"
    preview="Preview..."
  >
    <MjmlSection>
      <MjmlColumn>
        <MjmlButton
          padding="20px"
          backgroundColor="#346DB7"
          href={confirmEmailUrl}
        >
          Confirm Email
        </MjmlButton>
      </MjmlColumn>
    </MjmlSection>
  </Layout>
);
