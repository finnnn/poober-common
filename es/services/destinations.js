// import moment from 'moment';
import { isArray } from 'lodash';
import itsSet from 'its-set';
import moment from 'moment';

import TABLES from '../constants/tables';
// import { dateToMinsFromMidnight, dayOfWeek } from '../utils/time';
import { jsonToPoint } from '../utils/gis';
import businessHoursServiceFactory from './businessHours';
import usersServiceFactory from './users';
// import bookingsServiceFactory from './bookings';
import { int as parseInt } from '../utils/parse';

export default db => {
  const businessHoursService = businessHoursServiceFactory(db);
  const usersService = usersServiceFactory(db);

  const addMetaToOneDestination = async (destination) => {
    if (!destination) return null;
    const owner = await usersService.get(destination.owner);
    const businessHours = await businessHoursService.find({
      query: {
        entity_type: 'destination',
        entity_id: destination.id,
      },
    }); 
    const now = moment();

    return {
      ...destination,
      business_hours: businessHours.reduce((acc, hours) => ({
        ...acc,
        [parseInt(hours.day)]: {
          closing_time: hours.closing_time,
          opening_time: hours.opening_time,
          timezone: hours.timezone,
        },
      }), {}),
      owner,
      soonest_availability: (
        destination.soonest_availability &&
        moment(destination.soonest_availability).isAfter(now)
      ) ?
        moment(destination.soonest_availability).toDate() :
        null,
    };
  };

  const addMeta = async destination =>
    isArray(destination) ?
      await Promise.all(destination.map(d => addMetaToOneDestination(d))) :
      await addMetaToOneDestination(destination);

  const destinationsSelect = (callback = () => {}) => {
    const soonestAvailabilityQuery = db(TABLES.BOOKINGS)  
      .first()
      .select('end_at')
      .whereRaw(`destination = ${TABLES.DESTINATIONS}.id`)
      .andWhere({ active: true })
      .orderBy('end_at', 'desc')
      .as('soonest_availability');

    return db
      .with('destinations_enriched', (qb) => {
        qb.select(
          `${TABLES.DESTINATIONS}.*`,
          db.postgis.asText('location'),
          soonestAvailabilityQuery
        )
        .from(TABLES.DESTINATIONS)
        .groupBy(`${TABLES.DESTINATIONS}.id`)
        
        callback(qb);
      })
      .select('*')
      .orderBy('soonest_availability')
      .from('destinations_enriched');
  };

  return {
    find: async ({
      origin,
      // begin_at,
      // end_at,
      // timezone,
      limit,
      owner,
      urgent,
      not_owner,
    }) => {
      const destinationsQuery = destinationsSelect(qb => {
        if (itsSet(origin)) { 
          qb.orderByRaw(`location <-> st_setsrid(st_makepoint(${origin}),4326)`);
        }

        if (itsSet(owner)) {
          qb.andWhere({ owner: parseInt(owner) });
        }

        if (itsSet(not_owner)) {
          qb.whereNot({ owner: parseInt(not_owner) });
        }

        if (itsSet(urgent)) {
          qb.andWhere({ auto_accept_bookings: true });
        }
      });

      destinationsQuery.limit(parseInt(limit) || 100);

      const destinations = await destinationsQuery;

      return await addMeta(destinations);
    },
    
    create: async (data) => {
      const { name, location, auto_accept_bookings, owner, business_hours } = data;
      const created_at = new Date();
      const [destination] = await db(TABLES.DESTINATIONS).returning(['*', db.postgis.asText('location')]).insert({
        name,
        location: jsonToPoint({ db, point: location }),
        auto_accept_bookings,
        created_at,
        updated_at: created_at,
        owner,
      });

      if (itsSet(business_hours)) {
        await Promise.all(Object.keys(business_hours).map(day =>
          businessHoursService.create({
            entity_type: 'destination',
            entity_id: destination.id,
            timezone: business_hours[day].timezone,
            day: parseInt(day),
            opening_time: business_hours[day].opening_time,
            closing_time: business_hours[day].closing_time,
          })
        ));
      }

      const [updatedDestination] = await destinationsSelect(qb => {
        qb.where({ id: parseInt(destination.id) });
      });
      return addMeta(updatedDestination);
    },

    get: async(id) => {
      const [destination] = await destinationsSelect(qb => {
        qb.where({ id: parseInt(id) });
      });
      return addMeta(destination);
    },
    
    update: async (id, data) => {
      const { name, location, auto_accept_bookings, business_hours } = data;
      const [destination] = await db(TABLES.DESTINATIONS)
        .returning(['*', db.postgis.asText('location')])
        .where({ id: parseInt(id) })
        .update({
          name,
          location: jsonToPoint({ db, point: location }),
          auto_accept_bookings,
          updated_at: new Date(),
        });

      if (itsSet(business_hours)) {
        const previousBusinessHours = await businessHoursService.find({
          entity_type: 'destination',
          entity_id: destination.id,
        });

        const currentBusinessHours = await Promise.all(Object.keys(business_hours).map(day =>
          businessHoursService.createOrUpdate({
            entity_type: 'destination',
            entity_id: destination.id,
            timezone: business_hours[day].timezone,
            day: parseInt(day),
            opening_time: business_hours[day].opening_time,
            closing_time: business_hours[day].closing_time,
          })
        ));

        const daysToBeRemoved = previousBusinessHours
          .filter(hours => !currentBusinessHours.map(({ day }) => day).includes(hours.day));

        await Promise.all(daysToBeRemoved.map(({ id }) => businessHoursService.remove(id)));
      }

      const [updatedDestination] = await destinationsSelect(qb => {
        qb.where({ id: parseInt(id) });
      });

      return addMeta(updatedDestination);
    },

    remove: async (id) => {
      await db(TABLES.DESTINATIONS)
        .where({ id: parseInt(id) })
        .del();
    },

  };

};

