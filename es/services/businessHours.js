import TABLES from '../constants/tables';
import { int as parseInt } from '../utils/parse';

const find = db => ({ query }) =>
  db(TABLES.BUSINESS_HOURS)
    .select('*')
    .where(query || {});

const create = db => async (data) => {
  const { entity_type, entity_id, timezone, day, opening_time, closing_time } = data;
  const created_at = new Date();
  const [result] = await db(TABLES.BUSINESS_HOURS).returning('*').insert({
    entity_type,
    entity_id,
    timezone,
    day,
    opening_time,
    closing_time,
    created_at,
    updated_at: created_at,
  });
  return result;
};

const update = db => async (id, data) => {
  const { entity_type, entity_id, timezone, day, opening_time, closing_time } = data;
  const [result] = await db(TABLES.BUSINESS_HOURS)
    .returning('*')
    .where({ id: parseInt(id) })
    .update({
      entity_type,
      entity_id,
      timezone,
      day,
      opening_time,
      closing_time,
      updated_at: new Date(),
    });
  return result;
};

export default (db) => ({
  find: find(db),
  create: create(db),
  update: update(db),

  createOrUpdate: async (data) => {
    const { entity_type, entity_id, day } = data;

    const [existing] = await find(db)({ query: { entity_type, entity_id, day } });

    if (existing) {
      const [result] = await update(db)(existing.id, data);
      return result;
    }

    return await create(db)(data);
  },

  remove: async (id) => {
    await db(TABLES.BUSINESS_HOURS)
      .where({ id: parseInt(id) })
      .del();
  },

});
