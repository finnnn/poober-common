import { isArray, uniq } from 'lodash';
import itsSet from 'its-set';
import nanoid from 'nanoid';

import { int as parseInt } from '../utils/parse';
import { jsonToPoint } from '../utils/gis';
import TABLES from '../constants/tables';

const DEFAULT_OPTIONS = { blacklist: ['password'] };

const omit = (object, blacklistKeys) => {
  const result = { ...object };
  blacklistKeys.forEach(key => {
    delete result[key];
  });
  return result;
};

export default db => ({
  create: async (payload, opts) => {
    const options = { ...DEFAULT_OPTIONS, ...opts };
    const now = new Date();
    const [user] = await db(TABLES.USERS)
      .returning(['*', db.postgis.asText('location')])
      .insert({
        phone_validated: false,
        phone_validation_code: nanoid(),
        phone_validation_code_created_at: now,
        email_validated: false,
        email_validation_code: nanoid(),
        email_validation_code_created_at: now,
        created_at: now,
        updated_at: now,
        ...payload,
      });
    return omit(user, options.blacklist);
  },

  find: async (filters, opts) => {
    const options = { ...DEFAULT_OPTIONS, ...opts };

    const users = await db(TABLES.USERS)
      .select(['*', db.postgis.asText('location')])
      .where(filters);

    return users.map(user => omit(user, options.blacklist));
  },

  get: async (id, opts) => {
    const options = { ...DEFAULT_OPTIONS, ...opts };
    const multiple = isArray(id);
    
    const users = await db(TABLES.USERS)
      .select(['*', db.postgis.asText('location')])
      .whereIn('id', (multiple ? uniq(id) : [id]).map(parseInt));
    
    if (multiple) {
      return users.map(user => omit(user, options.blacklist));
    }
    else {
      const [user] = users;
      if (!user) return user;
      return omit(user, options.blacklist);
    }
  },
  
  update: async (id, data, opts) => {
    const options = { ...DEFAULT_OPTIONS, ...opts };
    const payload = {
      ...data,
      updated_at: new Date(),
    };
    
    if (itsSet(data.location)) {
      payload.location = jsonToPoint({ db, point: data.location });
    }

    const [user] = await db(TABLES.USERS)
      .returning(['*', db.postgis.asText('location')])
      .where({ id: parseInt(id) })
      .update(payload);

    return omit(user, options.blacklist);
  },

  remove: async (id) => {
    await db(TABLES.USERS)
      .where({ id: parseInt(id) })
      .del();
  },

});
 