import { isArray, uniq } from 'lodash';
import itsSet from 'its-set';

import usersServiceFactory from './users';
import TABLES from '../constants/tables';
import { int as parseInt } from '../utils/parse';

export default db => {

  const usersService = usersServiceFactory(db);

  const addMeta = async rating => {
    if (!rating) return null;
    const rater = await usersService.get(rating.rater);
    return {
      ...rating,
      rater,
    };
  };
  
  return {
    find: async ({ entity_id, entity_type, rater }) => {
      const query = db(TABLES.RATINGS).select('*');

      if (itsSet(entity_id)) {
        query.where('entity_id', parseInt(entity_id));
      }

      if (itsSet(entity_type)) {
        query.where('entity_type', entity_type);
      }

      if (itsSet(rater)) {
        query.where('rater', parseInt(rater));
      }

      // query.limit(10);
      const ratings = await query;
      return await Promise.all(ratings.map(addMeta));
    },

    create: async ({ entity_type, entity_id, rater, rating, review }) => {
      const created_at = new Date();
      const [result] = await db(TABLES.RATINGS).returning('*').insert({
        entity_type,
        entity_id,
        rater,
        rating,
        review,
        created_at,
        updated_at: created_at,
      });
      return await addMeta(result);
    },

    get: async(id) => {
      const [rating] = await db(TABLES.RATINGS)
        .select('*')
        .whereIn('id', (isArray(id) ? uniq(id) : [id]).map(parseInt));
      return rating;
    },
    
    update: async (id, data) => {
      const [rating] = await db(TABLES.RATINGS)
        .returning('*')
        .where({ id: parseInt(id) })
        .update({
          ...data,
          updated_at: new Date(),
        });
      return await addMeta(rating);
    },

    remove: async (id) => {
      await db(TABLES.RATINGS)
        .where({ id: parseInt(id) })
        .del();
    },

  };
};
