import { isArray, uniq } from 'lodash';
import itsSet from 'its-set';

import bookingsServiceFactory from './bookings';
import TABLES from '../constants/tables';
import { INBOUND_BOOKING_REQUEST } from '../constants/notificationTypes';
import { int as parseInt } from '../utils/parse';

export default db => {

  const bookingsService = bookingsServiceFactory(db);

  const addMeta = async (notification) => {
    if (!notification) return null;
    const { type, payload } = notification;
    const jsonPayload = JSON.parse(payload);
    switch(type) {
      case INBOUND_BOOKING_REQUEST: {
        const booking = await bookingsService.get(jsonPayload.bookingId);
        return {
          ...notification,
          payload: {
            ...jsonPayload,
            booking,
          },
        };
      }
      default: {
        return { ...notification, payload: jsonPayload };
      }
    }
  };
  
  return {
    find: async ({ recipient, active }) => {
      const query = db(TABLES.NOTIFICATIONS).select('*');

      if (itsSet(recipient)) {
        query.where('recipient', parseInt(recipient));
      }

      if (itsSet(active)) {
        query.where('active', active);
      }

      // query.limit(10);
      const notifications = await query;
      return await Promise.all(notifications.map(addMeta));
    },

    create: async ({ recipient, type, payload }) => {
      const created_at = new Date();
      const [result] = await db(TABLES.NOTIFICATIONS).returning('*').insert({
        recipient,
        payload: JSON.stringify(payload),
        type,
        created_at,
        active: true,
        updated_at: created_at,
      });
      return await addMeta(result);
    },

    get: async(id) => {
      const notifications = await db(TABLES.NOTIFICATIONS)
        .select('*')
        .whereIn('id', (isArray(id) ? uniq(id) : [id]).map(parseInt));
      
      if (isArray(id)) {
        return await Promise.all(notifications.map(addMeta));
      }
      else {
        return await addMeta(notifications[0]);
      }
    },
    
    update: async (id, data) => {
      const query = db(TABLES.NOTIFICATIONS)
        .returning('*')
        .update({
          ...data,
          updated_at: new Date(),
        });
      
      if (isArray(id)) {
        const notifications = await query.whereIn('id', id.map(parseInt));
        return await Promise.all(notifications.map(addMeta));
      }
      else {
        const [notification] = await query.where({ id: parseInt(id) });
        return await addMeta(notification);
      }
    },

    remove: async (id) => {
      await db(TABLES.NOTIFICATIONS)
        .where({ id: parseInt(id) })
        .del();
    },

  };
};
