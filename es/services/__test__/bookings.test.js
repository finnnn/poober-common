const { test } = require('tap'); 
const moment = require('moment');
const tk = require('timekeeper');

const db = require('../../utils/db');
// const gisUtil = require('../../utils/gis');
const destinationUtil = require('../../utils/destination');
const bookingUtil = require('../../utils/booking');
const { roundDate } = require('../../utils/test');
const {
  DEFAULT_BOOKING_COMMENCEMENT_PERIOD_MINUTES,
  DEFAULT_BOOKING_PERIOD_MINUTES,
} = require('../../constants/timing');
const { EXECUTION_SUCCESSFUL } = require('../../constants/bookingStatuses');
const bookingsService = require('../bookings')(db);
const destinationsService = require('../destinations')(db);
const usersService = require('../users')(db);
const TABLES = require('../../constants/tables');

const testData = {};
let firstBookingCreationTime;
// const currentTime = new Date();

test('services/bookings', async (t) => {
  t.test('intialize db', async (t) => {
    db.initialize();
    t.end();
  });

  t.test('truncate tables', async (t) => {
    await db(TABLES.DESTINATIONS).truncate();
    await db(TABLES.BOOKINGS).truncate();
    await db(TABLES.BUSINESS_HOURS).truncate();
    t.end();
  });

  t.test('seed data', async t => {
    const host = await usersService.create({
      name: `HOST`,
    });

    const guestA = await usersService.create({
      name: `GUEST A`,
    });

    const guestB = await usersService.create({
      name: `GUEST B`,
    });

    const guestC = await usersService.create({
      name: `GUEST C`,
    });

    const destA = await destinationsService.create({
      name: `DEST A`,
      location: { lat: 2, lon: 0 },
      owner: host.id,
    });
    
    testData.host = host;
    testData.guestA = guestA;
    testData.guestB = guestB;
    testData.guestC = guestC;
    testData.destA = destA;

    t.end();
  });

  t.test('booking A', async (t) => {
    firstBookingCreationTime = new Date();
    testData.bookingA = await destinationUtil.book({
      destinationId: testData.destA.id,
      userId: testData.guestA.id,
    });

    t.equal(
      roundDate(testData.bookingA.begin_at), 
      roundDate(moment().add(DEFAULT_BOOKING_COMMENCEMENT_PERIOD_MINUTES, 'minutes').toDate())
    );

    t.equal(
      roundDate(testData.bookingA.end_at), 
      roundDate(
        moment(firstBookingCreationTime)
          .add(
            DEFAULT_BOOKING_COMMENCEMENT_PERIOD_MINUTES + DEFAULT_BOOKING_PERIOD_MINUTES,
            'minutes'
          )
          .toDate()
      )
    );

    t.end();
  });

  t.test('booking B', async (t) => {
    testData.bookingB = await destinationUtil.book({
      destinationId: testData.destA.id,
      userId: testData.guestB.id,
    });

    t.equal(
      roundDate(testData.bookingB.begin_at), 
      roundDate(
        moment(firstBookingCreationTime)
          .add(
            DEFAULT_BOOKING_COMMENCEMENT_PERIOD_MINUTES + DEFAULT_BOOKING_PERIOD_MINUTES,
            'minutes'
          )
          .toDate()
      )
    );

    t.equal(
      roundDate(testData.bookingB.end_at), 
      roundDate(
        moment(firstBookingCreationTime)
          .add(
            DEFAULT_BOOKING_COMMENCEMENT_PERIOD_MINUTES + DEFAULT_BOOKING_PERIOD_MINUTES * 2,
            'minutes'
          )
          .toDate()
      )
    );

    t.end();
  });

  t.test('booking C', async (t) => {
    testData.bookingC = await destinationUtil.book({
      destinationId: testData.destA.id,
      userId: testData.guestC.id,
    });

    t.equal(
      roundDate(testData.bookingC.begin_at), 
      roundDate(
        moment(firstBookingCreationTime)
          .add(
            DEFAULT_BOOKING_COMMENCEMENT_PERIOD_MINUTES + DEFAULT_BOOKING_PERIOD_MINUTES * 2,
            'minutes'
          )
          .toDate()
      )
    );

    t.equal(
      roundDate(testData.bookingC.end_at), 
      roundDate(
        moment(firstBookingCreationTime)
          .add(
            DEFAULT_BOOKING_COMMENCEMENT_PERIOD_MINUTES + DEFAULT_BOOKING_PERIOD_MINUTES * 3,
            'minutes'
          )
          .toDate()
      )
    );

    t.end();
  });

  t.test('booking A execution', async (t) => {
    const bookingA = await bookingsService.update(testData.bookingA.id, {
      status: EXECUTION_SUCCESSFUL,
    });

    t.equal(bookingA.status, EXECUTION_SUCCESSFUL);

    await bookingUtil.syncPeerBookings(bookingA, db);

    const bookingB = await bookingsService.get(testData.bookingB.id);

    t.equal(
      roundDate(bookingB.begin_at),
      roundDate(
        moment(bookingA.created_at)
          .add(
            DEFAULT_BOOKING_COMMENCEMENT_PERIOD_MINUTES,
            'minutes'
          )
          .toDate()
      )
    );

    const bookingC = await bookingsService.get(testData.bookingC.id);

    t.equal(
      roundDate(bookingC.begin_at),
      roundDate(
        moment(bookingB.begin_at)
          .add(DEFAULT_BOOKING_PERIOD_MINUTES, 'minutes')
          .toDate()
      )
    );

    t.equal(
      roundDate(bookingC.end_at), 
      roundDate(
        moment(bookingB.end_at)
          .add(DEFAULT_BOOKING_PERIOD_MINUTES, 'minutes')
          .toDate()
      )
    );

    t.end();
  });

  t.test('booking B execution', async (t) => {
    const bookingB = await bookingsService.update(testData.bookingB.id, {
      status: EXECUTION_SUCCESSFUL,
    });

    t.equal(bookingB.status, EXECUTION_SUCCESSFUL);

    tk.travel(moment().add(DEFAULT_BOOKING_COMMENCEMENT_PERIOD_MINUTES + 1, 'minutes').toDate());

    await bookingUtil.syncPeerBookings(bookingB, db);

    const bookingC = await bookingsService.get(testData.bookingC.id);

    t.equal(
      roundDate(bookingC.begin_at),
      roundDate(new Date())
    );

    tk.reset()

    t.end();
  });

  t.test('close DB', (t) => {
    db.destroy(() => {
      t.end();
    });
  });

  t.end();
});