import { isArray, uniq } from 'lodash';

import TABLES from '../constants/tables';
import { int as parseInt } from '../utils/parse';

export default db => ({
  find: async ({ user }) => {
    const query = db(TABLES.SETTINGS_NOTIFICATIONS).select('*').where('user', user);
    const settingsNotifications = await query;
    return settingsNotifications;
  },

  create: async ({ user }) => {
    const [result] = await db(TABLES.SETTINGS_NOTIFICATIONS).returning('*').insert({ user });
    return result;
  },

  get: async(id) => {
    const [settingNotification] = await db(TABLES.SETTINGS_NOTIFICATIONS)
      .select('*')
      .whereIn('id', (isArray(id) ? uniq(id) : [id]).map(parseInt));
    return settingNotification;
  },

  remove: async (id) => {
    await db(TABLES.SETTINGS_NOTIFICATIONS)
      .where({ id: parseInt(id) })
      .del();
  },
});
