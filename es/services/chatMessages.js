import TABLES from '../constants/tables';
import usersServiceFactory from './users';
import { int as parseInt } from '../utils/parse';

export default db => {
  const usersService = usersServiceFactory(db);

  const formatMessage = async message => {
    if (!message) return null;
    const sender = await usersService.get(message.sender);
    const recipient = await usersService.get(message.recipient);
    return {
      ...message,
      sender,
      recipient,
    };
  };

  const formatMessages = async messages => {
    const users = await usersService.get(
      messages.reduce((acc, { sender, recipient }) =>
        acc.concat([sender, recipient]),
        []
      )
    );
    return messages.map(message => {
      return {
        ...message,
        sender: users.filter(({ id }) => id === message.sender)[0],
        recipient: users.filter(({ id }) => id === message.recipient)[0],
      };
    });
  };

  return {
    find: async ({ userA, userB }) => {
      const query = db(TABLES.CHAT_MESSAGES).select('*');

      query.andWhere(function() {
        this.orWhere({
          recipient: parseInt(userA),
          sender: parseInt(userB),
        });
        this.orWhere({
          recipient: parseInt(userB),
          sender: parseInt(userA),
        });
      });

      // query.limit(limit);

      const messages = await query;
      return formatMessages(messages);
    },

    create: async (data) => {
      const { text, sender, recipient, created_at } = data;
      const createdAt = created_at || new Date();
      const [result] = await db(TABLES.CHAT_MESSAGES).returning('*').insert({
        text,
        sender,
        recipient,
        created_at: createdAt,
        updated_at: createdAt,
      });
      return formatMessage(result);
    },

    get: async(id) => {
      const [message] = await db(TABLES.CHAT_MESSAGES)
        .select()
        .where({ id: parseInt(id) });
      return formatMessage(message);
    },
    
    update: async (id, { text }) => {
      const [message] = await db(TABLES.CHAT_MESSAGES)
        .returning('*')
        .where({ id: parseInt(id) })
        .update({
          text,
          updated_at: new Date(),
        });
      return formatMessage(message);
    },

    remove: async (id) =>
      await db(TABLES.CHAT_MESSAGES)
        .where({ id: parseInt(id) })
        .del(),

  };
};
