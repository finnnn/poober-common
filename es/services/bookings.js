import moment from 'moment';
import itsSet from 'its-set';
import { isArray } from 'lodash';

import TABLES from '../constants/tables';
import {
  COMMENCEMENT_PENDING,
  COMMENCEMENT_SUCCESSFUL,
  APPROVAL_PENDING,
  EXECUTION_SUCCESSFUL,
} from '../constants/bookingStatuses';
import destinationsServiceFactory from './destinations';
import usersServiceFactory from './users';
import { int as parseInt } from '../utils/parse';
import { DEFAULT_BOOKING_PERIOD_MINUTES } from '../constants/timing';

export default db => {
  const destinationsService = destinationsServiceFactory(db);
  const usersService = usersServiceFactory(db);

  const addMeta = async booking => {
    if (!booking) return null;
    const destination = await destinationsService.get(booking.destination);
    const requester = await usersService.get(booking.requester);
    return { ...booking, destination, requester };
  };

  return {
    find: async ({ requester, host, active, destination, day, begin_at, end_at, created_at }) => {
      const query = db(TABLES.BOOKINGS).select(`${TABLES.BOOKINGS}.*`);

      if (itsSet(day)) {
        query.where('day', day);
      }

      if (itsSet(destination)) {
        query.where('destination', parseInt(destination));
      }

      if (itsSet(requester)) {
        query.where('requester', parseInt(requester));
      }

      if (itsSet(host)) {
        query
          .join(
            TABLES.DESTINATIONS,
            `${TABLES.DESTINATIONS}.id`,
            `${TABLES.BOOKINGS}.destination`
          )
          .where(`${TABLES.DESTINATIONS}.owner`, parseInt(host));
      }

      if (itsSet(active)) {
        query.where('active', active);
      }

      if (itsSet(begin_at)) {
        if (isArray(begin_at)) {
          query.where('begin_at', ...begin_at);
        }
        else {
          query.where('begin_at', begin_at);
        }
      }

      if (itsSet(end_at)) {
        if (isArray(end_at)) {
          query.where('end_at', ...end_at);
        }
        else {
          query.where('end_at', end_at);
        }
      }

      if (itsSet(created_at)) {
        if (isArray(created_at)) {
          query.where('created_at', ...created_at);
        }
        else {
          query.where('created_at', created_at);
        }
      }

      // query.limit(10);

      const bookings = await query;

      return Promise.all(bookings.map(addMeta));
    },

    create: async (data) => {
      const { requester, begin_at, end_at, destination, status = APPROVAL_PENDING } = data;
      const created_at = new Date();
      const [result] = await db(TABLES.BOOKINGS).returning('*').insert({
        requester,
        begin_at,
        end_at,
        begin_day: moment(begin_at).day(),    
        destination,
        status,
        created_at,
        updated_at: created_at,
        active: [
          APPROVAL_PENDING,
          COMMENCEMENT_PENDING,
          COMMENCEMENT_SUCCESSFUL,
          EXECUTION_SUCCESSFUL,
        ].includes(status),
      });
      return addMeta(result);
    },

    get: async(id) => {
      const [booking] = await db(TABLES.BOOKINGS)
        .select(`${TABLES.BOOKINGS}.*`)
        .where({ id: parseInt(id) });
      return addMeta(booking);
    },
    
    update: async (id, data) => {
      const payload = {
        ...data,
        updated_at: new Date(),
      };

      if (itsSet(data.status)) {
        payload.active = [
          APPROVAL_PENDING,
          COMMENCEMENT_PENDING,
          COMMENCEMENT_SUCCESSFUL,
        ].includes(data.status);
      }

      if (itsSet(data.begin_at)) {
        payload.begin_day = moment(data.begin_at).day();
        payload.end_at = moment(data.begin_at).add(DEFAULT_BOOKING_PERIOD_MINUTES, 'minutes').toDate();
      }

      const bookingsQuery = db(TABLES.BOOKINGS)
        .returning('*');

      const multiple = isArray(id);

      if (multiple) {
        bookingsQuery.whereIn('id', id.map(i => parseInt(i)));
      } else {
        bookingsQuery.where({ id: parseInt(id) });
      }

      const bookings = await bookingsQuery.update(payload);

      return await (multiple ? Promise.all(bookings.map(booking => addMeta(booking))) : addMeta(bookings[0]));
    },

    remove: async (id) =>
      await db(TABLES.BOOKINGS)
        .where({ id: parseInt(id) })
        .del(),

  };
};
