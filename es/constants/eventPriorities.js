const LOW = 'LOW';
const MODERATE = 'MODERATE';
const DEFAULT = 'DEFAULT';
const HIGH = 'HIGH';
const CRITICAL = 'CRITICAL';

export default {
  LOW,
  MODERATE,
  DEFAULT,
  HIGH,
  CRITICAL,
};
