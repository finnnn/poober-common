export const BOOKINGS = 'public.bookings';
export const BUSINESS_HOURS = 'public.business_hours';
export const CHAT_MESSAGES = 'public.chat_messages';
export const DESTINATIONS = 'public.destinations';
export const MIGRATIONS = 'public.migrations';
export const NOTIFICATIONS = 'public.notifications';
export const RATINGS = 'public.ratings';
export const USERS = 'public.users';
export const SETTINGS_NOTIFICATIONS = 'public.user_has_notification_types';

export default {
  BOOKINGS,
  BUSINESS_HOURS,
  CHAT_MESSAGES,
  DESTINATIONS,
  MIGRATIONS,
  NOTIFICATIONS,
  RATINGS,
  SETTINGS_NOTIFICATIONS,
  USERS,
};
