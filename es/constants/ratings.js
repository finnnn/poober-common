const TERRIBLE = 'TERRIBLE';
const BAD = 'BAD';
const DECENT = 'DECENT';
const GOOD = 'GOOD';
const AWESOME = 'AWESOME';

export default {
  TERRIBLE,
  BAD,
  DECENT,
  GOOD,
  AWESOME,
};
