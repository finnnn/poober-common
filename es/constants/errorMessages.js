import {
  APPROVAL_DECLINED,
  APPROVAL_EXPIRED,
  COMMENCEMENT_PROBLEMATIC,
} from 'poober-common/cjs/constants/bookingStatuses';

export const BOOKING_STATUS_CHANGE = {
  [APPROVAL_DECLINED]: 'The host has declined your request. Select again from venues that will automatically accept your booking.',
  [APPROVAL_EXPIRED]: 'The host is unresponsive. Select again from venues that will automatically accept your booking.',
  [COMMENCEMENT_PROBLEMATIC]: 'Our condolences that you are having troubles. Select again from venues that will automatically accept your booking.',
};