const actions = require('../constants/actions');

export default (state = {}, action) => {
  switch (action.type) {
    case actions.auth.AUTH_SIGNIN_SUCCEEDED:
    case actions.auth.AUTH_SIGNUP_SUCCEEDED:
    case actions.auth.AUTH_PING_SUCCEEDED: {
      return action.payload;
    }
    case actions.auth.AUTH_SIGNOUT_SUCCEEDED: {
      return {};
    }
    default: {
      return state;
    }
  }
}
