import { findIndex, uniqBy } from 'lodash';

const actions = require('../constants/actions');

export default (state = [], action) => {
  switch (action.type) {
    case actions.destinations.DESTINATION_CREATE_SUCCEEDED: {
      return state.concat(action.payload);
    }
    case actions.destinations.DESTINATIONS_LIST_SUCCEEDED: {
      return uniqBy(state.concat(action.payload), 'id');
    }
    case actions.destinations.DESTINATION_UPDATE_SUCCEEDED: {
      const index = findIndex(state, { id: action.payload.id });
      const nextState = [ ...state ];
      nextState[index] = action.payload;
      return nextState;
    }
    case actions.destinations.DESTINATION_REMOVE_SUCCEEDED: {
      const index = findIndex(state, { id: action.payload.id });
      return [ ...state.slice(0, index), ...state.slice(index + 1) ];
    }
    case 'CLEAR_DATA': {
      return [];
    }
    default: {
      return state;
    }
  }
}
