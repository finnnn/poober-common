import { findIndex, uniqBy } from 'lodash';
import actions from '../constants/actions';

export default (state = {}, action) => {
  const { type, payload } = action;
  switch (type) {
    case actions.chat.CHAT_MESSAGE_CREATE_SUCCEEDED: {
      const nextState = { ...state };
      const otherPartyId = payload.outbound ? payload.recipient.id : payload.sender.id;
      nextState[otherPartyId] = nextState[otherPartyId] || [];
      nextState[otherPartyId].push(payload);
      return nextState;
    }
    case actions.chat.CHAT_MESSAGES_LIST_SUCCEEDED: {
      const nextState = { ...state };
      nextState[payload.recipient] = nextState[payload.recipient] || [];
      nextState[payload.recipient] = uniqBy(nextState[payload.recipient].concat(payload.messages), 'id');
      return nextState;
    }
    case actions.chat.CHAT_MESSAGE_UPDATE_SUCCEEDED: {
      const nextState = { ...state };
      const otherPartyId = payload.outbound ? payload.recipient : payload.sender;
      nextState[otherPartyId] = nextState[otherPartyId] || [];
      const index = findIndex(nextState[otherPartyId], { id: action.payload.message.id });
      nextState[otherPartyId] = nextState[otherPartyId][index] = {
        ...nextState[otherPartyId][index],
        ...payload.message
      };
      return nextState;
    }
    case actions.chat.CHAT_MESSAGE_REMOVE_SUCCEEDED: {
      const nextState = { ...state };
      nextState[payload.recipient] = nextState[payload.recipient] || [];
      const index = findIndex(nextState[payload.recipient], { id: action.payload.message.id });
      nextState[payload.recipient] = nextState[payload.recipient] = [
        ...nextState[payload.recipient].slice(0, index),
        ...nextState[payload.recipient].slice(index + 1),
      ];
      return nextState;
    }
    default: {
      return state;
    }
  }
}
