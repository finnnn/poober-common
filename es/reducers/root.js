import { combineReducers } from 'redux';

import authReducer from '../reducers/auth';
import destinationsReducer from '../reducers/destinations';
import bookingsReducer from '../reducers/bookings';
import venuesReducer from '../reducers/venues';

export default combineReducers({
  auth: authReducer,
  destinations: destinationsReducer,
  bookings: bookingsReducer,
  venues: venuesReducer,
});