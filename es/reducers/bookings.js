import { findIndex, uniqBy } from 'lodash';
import actions from '../constants/actions';

export default (state = [], action) => {
  switch (action.type) {
    case actions.bookings.BOOKING_CREATE_SUCCEEDED: {
      return state.concat(action.payload);
    }
    case actions.bookings.BOOKINGS_LIST_SUCCEEDED: {
      return uniqBy(state.concat(action.payload), 'id');
    }
    case actions.bookings.BOOKING_UPDATE_SUCCEEDED: {
      const index = findIndex(state, { id: action.payload.id });
      const nextState = [ ...state ];
      nextState[index] = action.payload;
      return nextState;
    }
    case actions.bookings.BOOKING_READ_SUCCEEDED: {
      const index = findIndex(state, { id: action.payload.id });
      const nextState = [ ...state ];
      if (index !== -1) {
        nextState[index] = action.payload;
      }
      else {
        nextState.push(action.payload);
      }
      return nextState;
    }
    case actions.bookings.BOOKING_USER_LOCATION_UPDATED: {
      const index = findIndex(state, { id: action.payload.bookingId });
      const nextState = [ ...state ];
      nextState[index].userLocation = action.payload.location;
      return nextState;
    }
    case actions.bookings.BOOKING_REMOVE_SUCCEEDED: {
      const index = findIndex(state, { id: action.payload.id });
      return [ ...state.slice(0, index), ...state.slice(index + 1) ];
    }
    case 'CLEAR_DATA': {
      return [];
    }
    default: {
      return state;
    }
  }
}
