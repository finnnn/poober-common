import { uniqBy, sortBy, findIndex, isArray } from 'lodash';

const actions = require('../constants/actions');

const filter = state => uniqBy(state, 'id');
const sort = state => sortBy(state, 'created_at');
const replace = (collection, item) => {
  const index = findIndex(collection, { id: item.id });
  const nextCollection = [ ...collection ];
  nextCollection[index] = item;
  return nextCollection;
};

export default (state = [], action) => {
  switch (action.type) {
    case actions.notifications.NOTIFICATION_CREATE_SUCCEEDED: {
      return sort(filter(state.concat(action.payload)));
    }
    case actions.notifications.NOTIFICATIONS_LIST_SUCCEEDED: {
      return sort(filter(state.concat(action.payload)));
    }
    case actions.notifications.NOTIFICATION_UPDATE_SUCCEEDED: {
      if (isArray(action.payload)) {
        const nextState = action.payload.reduce((acc, item) => replace(acc, item), state);
        return sort(filter(nextState));
      }
      return sort(filter(replace(state, action.payload)));
    }
    case 'CLEAR_DATA': {
      return [];
    }
    default: {
      return state;
    }
  }
}
